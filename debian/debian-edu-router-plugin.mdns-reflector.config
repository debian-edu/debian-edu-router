#!/bin/bash

# Copyright (C) 2023 Pädagogisches Landesinstitut Rheinland-Pfalz
# Copyright (C) 2023 Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
# Copyright (C) 2023 Daniel Teichmann <daniel.teichmann@das-netzwerkteam.de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

set -e

. /usr/share/debconf/confmodule || exit 255

common_file="/usr/share/debian-edu-router/debian-edu-router.common"

# Load common functions, variables and stuff.
if [ -s "$common_file" ]; then
	source "$common_file"
else
	echo "Could not load common file at "$common_file"."
	exit 0;
fi

# prepare debconf
export DC_PRIO_LOW="medium"
export DC_PRIO_HIGH="high"

db_version 2.0
db_capb backup escape

if [ -e /etc/debian-edu/router.conf ]; then
	source /etc/debian-edu/router.conf
fi
PRODUCTNAME="${PRODUCTNAME:-"Debian Edu Router"}"
PRODUCTNAME_PLUGIN_SUFFIX="${PRODUCTNAME_PLUGIN_SUFFIX:-"Plugin"}"
PRODUCTNAME_PLUGIN="${PRODUCTNAME_PLUGIN:-"${PRODUCTNAME} ${PRODUCTNAME_PLUGIN_SUFFIX}: mDNS-Reflector"}"
PACKAGE_NAME="debian-edu-router-plugin.mdns-reflector"
db_title "${PRODUCTNAME_PLUGIN}"

# If we don't run this script for the first time, make sure we know what IP
# proto versions are enabled for non-network configuration steps, what internal
# networks are enabled and what internal networks have static IP address set.
#
# This is really important for cases where the sysadmin chooses to skip network
# settings. We can't populate the IPV4 and IPV6 variables from within the while
# loop, so let's preset these values in case networking set up gets skipped.
#
# Sets IPV4 and IPV6 bool variables.
parse_ip_versions

if [ "${CONFIGURE_ONLY}" = "NOT_IMPLEMENTED" ]; then
	STATE=2
	QCOUNT=2
elif [ "${CONFIGURE_ONLY}" = "ONOFF" ]; then
	STATE=0
	QCOUNT=0
elif [ "${SKIP_DEBCONF_QUESTIONS_CONFIG}" = "1" ]; then
	STATE=999
	QCOUNT=999
else
	STATE=0
	QCOUNT=1
fi

# Define a starting point that cannot be skipped by going back
BACKSTOP=${STATE}

debug_log "Picked up the following debconf-question state-machine settings for ${PACKAGE_NAME}.config:"
debug_log "  - SKIP_DEBCONF_QUESTIONS_CONFIG='${SKIP_DEBCONF_QUESTIONS_CONFIG}'."
debug_log "  - CONFIGURE_ONLY=               '${CONFIGURE_ONLY}'."
debug_log "  - STATE=                        '${STATE}'."
debug_log "  - QCOUNT=                       '${QCOUNT}'."
debug_log "  - BACKSTOP=                     '${BACKSTOP}'."
debug_log "  - Script arguments=             '${0} $@'."

# Failure counters: Some question don't allow empty input or similar.
# We can't re-ask questions endlessly, but need to bail out at some point.
FCOUNTER=0

# default behaviour should be that steps go UP rather than down.
# we have this variable to track in which direction we are currently going
# for example if the user wants to backup a step at step 17
# and step 16 should be skipped (for whatever reason) then we should move on
# to step *15* instead of step 17!
STATE_DIRECTION=1

# setup milestones
# when configuring a new milestone, please test if backing up from that
# milestone does work as expected!
ONOFF_QUESTION=0
FIRST_QUESTION=1

function statemachine() {
	while [ ${STATE} -gt -1 ] && [ ${STATE} -le ${QCOUNT} ] && [ ${STATE} -ge ${BACKSTOP} ]; do
		debug_log "We are currently at step ${cyan}$STATE${normal}."

		# ask questions
		case "${STATE}" in
			#
			# mDNS reflector (Service Discovery accross network borders)
			#

			0)
				# PLUGIN ENABLED?
				db_input ${DC_PRIO_HIGH} debian-edu-router-plugin.mdns-reflector/mdns-reflector-enabled || true
				;;

			1)
				db_get debian-edu-router-config/net-int-supportednetworks
				supported_internal_networks="${RET}"

				# Don't bother asking if only one (or less) internal network is available.
				if [[ $(echo "$supported_internal_networks" | wc -w) -le 1 ]]; then
					db_set debian-edu-router-plugin.mdns-reflector/service-mdns-reflector-networks ""
					STATE=$(($STATE + $STATE_DIRECTION))
					continue
				fi

				# Asking for participating networks
				db_subst debian-edu-router-plugin.mdns-reflector/service-mdns-reflector-networks choices "${supported_internal_networks}"
				db_input ${DC_PRIO_HIGH} debian-edu-router-plugin.mdns-reflector/service-mdns-reflector-networks || true
				;;

			# not implemented yet...
			2)
				db_input ${DC_PRIO_HIGH} debian-edu-router-config/not-implemented-yet || true
				;;

			# Used for skipping/aborting questions entirely.
			999)
				;;

			*)
				echo "Unknown state ${STATE}!";
				exit 255
				;;

		esac

		if db_go || [ "$BYPASS_DB_GO" == true ]; then
			# evaluate answers
			case "${STATE}" in
				#
				# mDNS reflector (Service Discovery accross network borders)
				#

				0)
					# PLUGIN ENABLED?
					db_get debian-edu-router-plugin.mdns-reflector/mdns-reflector-enabled || true
					if [ "${RET}" = "false" ]; then
						STATE=999
						continue
					fi
					;;

				1)
					db_get debian-edu-router-plugin.mdns-reflector/service-mdns-reflector-networks || true
					service_firewall_networks_mdns_reflection="${RET}"

					# Don't allow less than 2 selected networks.
					items=($service_firewall_networks_mdns_reflection)
					if [ "${#items[@]}" -lt 2 ] && ! [ "${#items[@]}" -eq 0 ]; then
						FCOUNTER=$((${FCOUNTER}+1))
						bailout_on_too_many_failures debian-edu-router-plugin.mdns-reflector/service-mdns-reflector-networks ${FCOUNTER} 5

						# Let the user try again
						STATE_DIRECTION=-1
						STATE=$(($STATE + $STATE_DIRECTION))
					else
						FCOUNTER=0
					fi
					;;

				# not implemented yet...
				2)
					;;

				# Used for skipping/aborting questions entirely.
				999)
					;;

			esac

			# last question was ok, so go up.
			# except if we just simulated that step...
			if [ "$BYPASS_DB_GO" == true ]; then
				STATE_DIRECTION=1
				STATE=$ORIGIN_STATE
			else
				STATE_DIRECTION=1
				STATE=$(($STATE + $STATE_DIRECTION))
			fi

			# reset.
			BYPASS_DB_GO=false
		else
			# last question was not ok (user wants to backup), so go *DOWN* a step.
			# and if the next step should be skipped, go down another step.
			# and if that step should also be skipped, go down another step.
			# and so on and so on...
			STATE_DIRECTION=-1
			STATE=$(($STATE + $STATE_DIRECTION))
			debug_log "Backing up to step ${cyan}${STATE}${green}..."
		fi
	done

	if [ $STATE -lt $BACKSTOP ]; then
		# user went backwards from beyond BACKSTOP/entry point, abort package configuration.
		debug_log "User tried to backup beyond BACKSTOP/entry point... Exiting."
		exit 10
	fi
}

function main() {
	statemachine
}

if [[ "$1" == "configure" ]] && [[ -n "$2" ]]; then
	main
elif [[ "$1" == "configure" ]] && [[ "$2" == "debian-edu-router-reconfigured" ]]; then
	debug_log "config script was called via dpkg-trigger, specifically 'debian-edu-router-reconfigured' trigger."
	main
elif [[ "$1" == "reconfigure" ]]; then
	debug_log "config script was called via dpkg-reconfigure."
	main
else
	debug_log "config script was probably called to preconfigure package, skipping..."
	exit 0
fi

# dh_installdeb will replace this with shell code automatically
# generated by other debhelper scripts.

#DEBHELPER#

debug_log "Finished .config stage of ${PACKAGE_NAME}."

exit 0
