# debian-edu-router - i18n documentation

The debian-edu-router package is 100% pre-seedable via debconf. When
installing the ``debian-edu-router-*`` packages, the resulting firewall
/router / contentfilter system becomes a highly configurable installation
and so, there are tons of debconf dialogs to translate.

Thanks in advance for facing this challenge as a translator. Your
translation work will be much appreciated.

Furthermore, the debian-edu-router package is a native Debian package and
an installer for a Debian system (using [FAI](https://fai-project.org)).
So, there is more to translate than just ``debian/po/templates.pot``.

Also, as a translator you may recommend the best keymap setting and
locale setting for your language and your region to be used by the FAI
installer during the Debian Edu Router installation. Receiving your input
on this will also be much appreciated.

## Guidelines for localizing Debian Edu Router

You can e.g. use poedit for working with gettext's ``.po`` files shipped
in debian-edu-router. For the ``.po`` files in ``debian/po/`` please
adhere to the Debian i18n workflow as described here:  
https://www.debian.org/doc/manuals/developers-reference/best-pkging-practices.html#general-recommendations-for-authors-and-translators

Following, you find the guidelines for a complete localiztion of
debian-edu-router into your locale:

### Actual Translation Work

  * Add/update ``debian/po/<lang>.po`` from the ``templates.pot`` file in the same
    folder. (This will take some time, thanks!)
  * Some more ``.po`` files you will find in ``po/<lang>/*.po``. Please also
    translate those if you can. (This will be a much quicker task).

### Localizing the Debian Edu Router FAI Installation Process

  * Make up a good FAI language class name, such as ``LANG_FRBELGIAN``,
    ``LANG_GERMAN`` or ``LANG_SWISSGERMAN``. Furtheron referred to as
    ``LANG_<LOCALE>``.

  * Provide a variable file ``LANG_<LOCALE>.var`` for the FAI installer
    containing these settings:

    ```
    LANG=$(echo $LANG)
    KEYMAP=??? (FIXME: How can one obtain the current kernel keymap in use)
    ```

    See ``fai/config/classes/LANG_*.var`` for examples.

  * Please take a look at ``fai/config/debconf/LANG_*`` and provide
    language-specific and region-specific settings as preseedings for
    packages already listed in LANG_* (e.g. LANG_GERMAN is a good example
    to look at). That is, use LANG_GERMAN as a template and adjust its
    values to your own regional settings. If you installed your own
    localized Debian system on your computer, you can peek at
    ``/var/cache/debconf/config.dat`` to derive the required values.

  * Add two installation profiles (minimal, full) to
    ``fai/config/class/z20_debian-edu-router.profile`` (or provide a
    snippet for us to copy+paste it there). The installation profile
    should have the ``LANG_<LOCALE>`` in the Classes: field.


## How to Submit my Localization Contribution?

You can either send all the files to add/update to the Debian bugtracker
system or file a merge request on Salsa:  
https://salsa.debian.org/debian-edu/debian-edu-router
