Template: debian-edu-router-plugin.content-filter/content-filter-enabled
Type: boolean
Default: false
_Description: Enable content filter plugin?

Template: debian-edu-router-plugin.content-filter/purging-warn-remaining-config
Type: error
_Description:
 All Content filter plugin specific service and firewall
 settings will be purged from the system.
 .
 Please check the remaining configuration files before restarting
 service 'e2guardian' and 'squid'.


# DNS ALIAS
Template: debian-edu-router-plugin.content-filter/dns-alias
Default: webcache
Type: string
_Description: DNS alias for content filter on internal networks:
 Please specify a DNS alias under which content filter can be reachable
 on internal networks.
 .
 It is recommended to add this DNS alias and its corresponding IP address to
 /etc/hosts.
 .
 This alias is an addition to this machine's hostname 'gateway'.

# DNS SERVERS
Template: debian-edu-router-plugin.content-filter/dns-servers
Type: string
Default: 10.0.2.2
_Description: DNS servers for resolving client hostnames:
 Please specify DNS servers to be used by the content filter for resolving
 client hostnames.
 .
 To resolve hostnames from Proxy*Client(.local) group objects, one or multiple
 DNS servers need to be queried.
 .
 This is usually the AD domain controller or, in Debian Edu, the Tjener server
 (10.0.2.2).


# WARN ABOUT SSL MITM CA CERT RECREATION.
Template: debian-edu-router-plugin.content-filter/warning-ssl-mitm-ca-cert-rewrite
Type: boolean
_Description: Warning: do you really wish to recreate the SSL root CA certificate?
 Please note that the old certificate will be INVALID.
 .
 You'll have to reinstall the new SSL certificate to every
 device in your network.
 .
 Devices which where not updated properly will warn about a unsecure connection
 (self-signed SSL certificate).
 .
 Please *BACKUP* the entire directory: '/etc/debian-edu-router/ssl/', *BEFORE* proceeding.


# WARN ABOUT OPEN NETWORKS
Template: debian-edu-router-plugin.content-filter/warning-networks-internet-allowed
Type: error
_Description: Warning: ${num_networks} network(s) configured for direct internet access.
 Please note that the following internal networks are configured to allow
 direct internet access:
 .
   ${list_networks}
 .
 Filtering internet traffic only makes sense if clients get blocked from
 direct internet access via firewall rules.
 .
 Please consider configuring this machine's firewall to block direct
 internet access for the listed internal networks.


# NETWORKS WITH HTTP CACHING PROXY ENABLED (SQUID)
Template: debian-edu-router-plugin.content-filter/service-httpproxy-networks-enabled
Type: multiselect
Choices: ${choices}
_Description: Networks that offer connecting to the internet via an http proxy?
 You can enable http caching proxy services on a selection of internal
 networks.
 .
 Internet access can be controlled (blocked/allowed) based on:
 .
   - Client IPs
   - Website IPs
   - Website URL paths (requires SSL MitM)
   - Deep content introspection (requires SSL MitM + Deep Content Filtering)
   - Access rules based on authentication of users and their groups memberships
 .
 The caching and website access control will be handled by the software
 'squid'.
 .
 If access gets blocked by the proxy, a warning/error page will be
 injected for HTTP connections, but HTTPS connections will simply
 be denied (ending up with a 'Connection refused' web browser error
 message). Injecting warning/error pages for HTTPS connections requires
 SSL MitM to be enabled in a later dialog.


# SQUID PORT EXPOSED TO CLIENTS
Template: debian-edu-router-plugin.content-filter/proxy-port-exposed-to-clients
Type: string
Default: 3128
_Description: HTTP Proxy Port exposed to Clients on the internal Networks:
 From the HTTP proxy port number, two other port numbers are derived,
 if transparent proxy mode is activated:
 .
   * HTTP  proxy port (transparent) -> HTTP proxy port + 1
   * HTTPS proxy port (transparent) -> HTTP proxy port + 2
 .
 Make sure those three server ports are not in use by other services on
 the this host's internal networks' IP addresses.


# TRANSPARENT PROXY MODE ENABLED?
Template: debian-edu-router-plugin.content-filter/service-httpproxy-transparent-mode-enabled
Type: boolean
_Description: Enable transparent proxy mode?
 With transparent proxy mode you can enforce clients connecting to Squid,
 using NAT, without them actually knowing it.
 .
 Your preferred way of connecting to a proxy should be using forward
 proxy mode. This is always activated. The client needs to be configured
 to actively use the http proxy.
 .
 Forward proxy mode can either be configured by finding the proxy automatically
 (using WPAD) or manually (e.g. group policies).
 .
 For both modes, the SSL-bumping root certificate (.crt file) must be installed
 in and trusted by the client's browser.
 .
 For further information see 'squid_ACLs_transparent_mode.md' and
 'squid_ACLs_explained.md'.


# APACHE2 HTTP PORT
Template: debian-edu-router-plugin.content-filter/apache2-http-port
Type: string
Default: 80
_Description: Please input the Apache2 HTTP port:
 Apache2 will be installed on this host. It will come with a very minimal
 preconfigured website which allows users to download the SSL CA certificate
 file, for example.
 .
 This field can't be left empty. Please use either a custom port or use the
 default of port 80 or 8080.
 .
 Please note: If you want to configure a reverse NAT, you'll have to choose a
 different port here. You can't use the same port for reverse NAT and Apache2.


# APACHE2 HTTPS PORT
Template: debian-edu-router-plugin.content-filter/apache2-https-port
Type: string
Default: 443
_Description: Please input the Apache2 HTTPS (SSL) port:
 Apache2 will be installed on this host. It will come with a very minimal
 preconfigured website which allows users to download the SSL CA certificate
 file, for example.
 .
 This field can be left empty. SSL (HTTPS) will then be deactivated for this
 host's Apache2 service.
 .
 The default port for HTTPS traffic is 443 or 4043.
 .
 Please note: If you want to configure a reverse NAT, you'll have  to choose a
 different port here. You can't use the same port for reverse NAT and Apache2.

# FIELD CAN'T BE EMPTY
Template: debian-edu-router-plugin.content-filter/cannot-be-empty
Type: error
_Description: This field can't stay empty.
 Please do provide an input now or else this configuration will be aborted
 after 5 tries.

Template: debian-edu-router-plugin.content-filter/parent-proxy
Type: string
Default:
_Description: Parent proxy <hostname>:<port> for accessing internet websites:
 The school network does not have direct internet access at all, but needs
 to communicate via a parent HTTP proxy (specified as <hostname>:<port>).
 .
 Leave empty if no parent proxy is required.


# SQUID: ENABLE SSL BUMPING FEATURE
# Source of 82.5 % percent HTTPS usage:
#   https://w3techs.com/technologies/details/ce-httpsdefault
Template: debian-edu-router-plugin.content-filter/service-httpproxy-enable-sslbumping
Type: boolean
_Description: Use SSL bumping and Deep Introspection?
 An http proxy's SSL Man-in-the-Middle feature is designed to intercept
 and decrypt HTTPS traffic to analyze the content and filter out
 malicious websites based on URL paths, keywords and heuristics.
 .
 After that, the website's data gets re-encrypted and forwarded to the
 intended destination (e.g. a user's webbrowser). This means you'll have
 to install a trusted root certificate (CA) on all client devices using
 the http proxy.
 .
 However, as HTTPS is designed to provide secure and private communication,
 using SSL MitM can raise security concerns and may require careful
 configuration to avoid compromising user privacy.
 .
 Please note that you want to have a powerful machine as your Debian Edu
 Router host as running SSL MitM with deep content introspection is
 heavily relying on CPU, I/O and network power, since zillions of SSL
 packets have to be intercepted, decrypted, analyzed, encrypted and
 forwarded. So please scale accordingly.

# SQUID: NETWORKS WITH SSL BUMPING SUPPORT
Template: debian-edu-router-plugin.content-filter/service-httpproxy-networks-with-sslbumping
Type: multiselect
Choices: ${choices}
_Description: For which networks should SSL bumping be enabled?
 For SSL-based HTTPS connections, http caching is not possible, the
 secured connections is simply forwarded by the proxy.
 .
 With SSL bumping you can enable caching of HTTPS website content and
 various other content related analysis features.
 .
 With SSL bumping enabled, all non-whitelisted traffic gets decrypted by
 the http proxy and can then be introspected in detail. Before handing the
 content over to the requesting client, the data will get re-encrypted
 via a site-specific certificate authority.
 .
 SSL bumping is required for URL-based (vs. site-based) content blocking
 and also for keyword based filtering of website content.
 .
 SSL bumping is also required for injecting error/warning pages when
 website access via HTTPS gets blocked by the http proxy.
 .
 NOTE: At least one network must be selected here. For disabling SSL bumping
 go back to the previous configuration dialog.

Template: debian-edu-router-plugin.content-filter/service-httpproxy-sslbumping-ca-org
Type: string
Default: Debian
_Description: Organization name:

Template: debian-edu-router-plugin.content-filter/service-httpproxy-sslbumping-ca-ou
Type: string
Default: Debian Edu
_Description: Organizational unit name:

Template: debian-edu-router-plugin.content-filter/service-httpproxy-sslbumping-ca-province
Type: string
Default: My Region
_Description: State or province:

Template: debian-edu-router-plugin.content-filter/service-httpproxy-sslbumping-ca-city
Type: string
Default: My City
_Description: Locality name / city:

Template: debian-edu-router-plugin.content-filter/service-httpproxy-sslbumping-ca-country
Type: string
Default: AU
_Description: Country code (two letters):

Template: debian-edu-router-plugin.content-filter/service-httpproxy-sslbumping-ca-cn
Type: string
Default: Debian Edu Router SSL-MitM CA
_Description: Common name (CN):

Template: debian-edu-router-plugin.content-filter/service-httpproxy-sslbumping-ca-email
Type: string
Default: siteadmin@myschool.edu
_Description: Email address for contacting the site admin:

# E2GUARDIAN: NETWORKS WITH DEEP CONTENT FILTERING
Template: debian-edu-router-plugin.content-filter/service-contentfilter-networks-enabled
Type: multiselect
Choices: ${choices}
_Description: For which networks shall deep content filtering be enabled?
 Deep content filtering means that HTTP and HTTPS based traffic
 between a web browser and a website gets introspected and analyzed in
 realtime.
 .
 Based on keywords, URL blocklists / allowlists, MIME types, etc. content
 can be filtered out and replaced by an error/warning page.
 .
 The deep content filtering will be handled by the software 'e2guardian'.

# E2GUARDIAN: Auto-refresh blacklists?
Template: debian-edu-router-plugin.content-filter/service-contentfilter-autorefresh-blacklist
Type: boolean
Default: false
_Description: Auto-refresh blacklists periodically?
 If you enable blacklists auto-refreshing here the router will periodically
 refresh the blacklists in '/var/lib/debian-edu-router/d-e-r-p.c-f/blacklists.d/'.
 .
 The e2guardian instance will be reloaded automatically. The default interval
 is set to be daily at night.
 .
 You can select the categories to blacklist in the loginmenu.
 .
 The blacklists are not maintained by Debian or Debian Edu (Router)
 maintainers. They are maintained by the University of Toulouse (check
 documentation for more info). You as the administrator are responsible for its
 content.

# SQUID: Auto-regenerate Proxy*Site.* IPs?
Template: debian-edu-router-plugin.content-filter/service-contentfilter-autoregenerate-ips
Type: boolean
Default: false
_Description: Auto-regenerate Proxy*Site.* IPs periodically?
 All websites, which are listed in Proxy*Site.*, will be resolved.
 The resolved IP addresses are then stored in Proxy*SiteIP.generated.
 .
 The underlying script will be executed daily.
 .
 For further information see 'squid_ACLs_transparent_mode.md'.
 .
 All documentation files can be found under:
 /usr/share/doc/debian-edu-router-plugin.content-filter/*
