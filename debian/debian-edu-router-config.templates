#
# Network Setup
#

Template: debian-edu-router-config/skip-networking-if-not-enough-ifaces-available
Type: boolean
_Description: Do you want to skip the Router's networking configuration?
 ERROR: Not enough usable network interfaces available for setting up the
 router!

Template: debian-edu-router-config/adopt-manual-uplink-settings
Type: boolean
_Description: Do you wish to get the Uplink network interface configured automagically?
 A working manually configured Uplink network connection has been detected:
 .
 ${uplink_info}
 .
 The settings above can be adopted automatically to the Uplink network
 interface. Please note:
 .
 - The file containing the definition of the interface listed above will be moved
 out of the way by adding a '.d-e-r.backup' suffix. E.g. /etc/network/interfaces.d-e-r.backup.
 .
 - Only basic IP setting options can be imported (DHCP or if static: address,
 submask, nameserver and gateway).
 .
 - The Router does not support more sophisticated options at the
 moment. e.g. VLAN, broadcast, (pre-)up, (pre-)down.
 .
 - The Router does not support a manually managed Uplink network
 interface at the moment.

Template: debian-edu-router-config/skip-networking-if-not-enough-ifaces-non-configured
Type: boolean
_Description: Do you want to skip the Router's networking configuration?
 ERROR: Not enough unconfigured network interfaces available for setting
 up the router!
 .
 The following interfaces were found already configured in files not
 managed by the Router:
 .
 ${non_d_e_r_ifaces}
 .
 Please consider unconfiguring these interfaces and re-try again.

Template: debian-edu-router-config/net-disconnect-all-interfaces
Type: error
_Description: Please plug a cable into the 'Uplink' interface.
 NOTE: For the requested step-by-step setup, please start with all
 network cables disconnected except for the external 'Uplink' interface.
 .
 You have ${num_tries} try left to unplug all network cables (except
 'Uplink') until the step-by-step setup will be aborted.

Template: debian-edu-router-config/net-failed-to-disconnect-all-interfaces
Type: error
_Description: Network cables still plugged in or no 'Uplink' interface
 ERROR: The networking cables were not unplugged and/or an 'Uplink'
 interface could not be determined. Please try again.

Template: debian-edu-router-config/ip-forwarding-consent
Type: select
__Choices: Yes, Abort
_Description: Do you want to enable IP packet forwarding?
 The Router requires IP packets to be forwarded back and forth between network
 interfaces by the kernel. This is mandatory and without it the router simply
 won't work. If you select 'Abort' this package will be left unconfigured. To
 undo its half-installed state, remove/purge it again.
 .
 IPv4 / IPv6 forwarding will only be enabled if the specific IP protocol
 version is configured and enabled.

Template: debian-edu-router-config/net-setup-mode
Type: select
__Choices: ALL-CONNECTED - all network cables are already connected (I know what I am doing), STEP-BY-STEP - connect network cables step-by-step and automatically assign network interfaces this way, OFFLINE-SETUP - network cables are not connected (this is an offline installation\, and yes\, I know what I am doing), SKIP-NETWORK-SETUP - don't configure network interface assignments for now\, at all
_Description: The network interface assignment method:
 You need to assign this host's network interfaces to network types
 supported by the Router. The network interface assignments can be
 done in three ways (plus a bail-out method).
 .
 (1) ALL-CONNECTED - All network interfaces are already connected and you
 know which interface you want to use for what network type.
 .
 (2) STEP-BY-STEP - Connect and assign network interfaces one by one.
 This will add dialogs between each interface assignment requesting to
 connect the network cable of the to-be-assigned network interface.
 .
 (3) OFFLINE-SETUP - No network interface is currently connected, allow
 configuration of network interfaces that currently don't have a network
 carrier. You also know which interface you want to use for what network
 type.
 .
 (4) SKIP-NETWORK-SETUP - Don't configure networking via this package, at
 all. Skip network configuration.

Template: debian-edu-router-config/net-ext-iface-uplink
Type: select
Choices: ${choices}
_Description: Which NIC shall be used as the 'Uplink' network interface?
 The following network interfaces on this system are connected to a
 network.
 .
 ${extended}

Template: debian-edu-router-config/net-ext-iface-uplink-assigned
Type: error
_Description: The external 'Uplink' interface has been successfully assigned.
 Network interface '${iface}' will be used as the host's external
 'Uplink' interface.

Template: debian-edu-router-config/net-int-supportednetworks
Type: multiselect
__Choices: OpenLAN, Education, Mgmt, School-Administration, WiFi-Students, WiFi-Teachers, WiFi-Guests, Printers
_Description: Supported internal school networks types:

Template: debian-edu-router-config/net-int-with-vlans
Type: boolean
Default: false
_Description: Shall VLANs be used on the internal network?

Template: debian-edu-router-config/net-connect-int-iface-vlan
Type: error
_Description: Please connect the network cable (VLAN trunk) of the 'VLAN' interface, now.

Template: debian-edu-router-config/net-int-iface-vlan
Type: select
Choices: ${choices}
_Description: Which NIC shall be used as VLAN interface?
 The following network interfaces on this system are connected to a
 network.
 .
 ${extended}

Template: debian-edu-router-config/net-int-iface-vlan-assigned
Type: error
_Description: The internal 'VLAN' interface has been successfully assigned.
 Network interface '${iface}' will be used to connect this host
 internally to all configured 'VLAN' networks.

Template: debian-edu-router-config/net-int-supportednetworks-via-vlan
Type: multiselect
Choices: ${choices}
_Description: Supported network types provided as VLANs:

# networks as VLANs on the internal network

Template: debian-edu-router-config/net-int-vlanid-openlan
Type: string
Default: 1
_Description: VLAN-ID for the 'OpenLAN' VLAN:

Template: debian-edu-router-config/net-int-vlanid-education
Type: string
Default: 2
_Description: VLAN-ID for the 'Education' VLAN:

Template: debian-edu-router-config/net-int-vlanid-mgmt
Type: string
Default: 3
_Description: VLAN-ID for the 'Mgmt' VLAN:

Template: debian-edu-router-config/net-int-vlanid-schooladministration
Type: string
Default: 4
_Description: VLAN-ID for the 'School-Administration' VLAN:

Template: debian-edu-router-config/net-int-vlanid-wifistudents
Type: string
Default: 21
_Description: VLAN-ID for the 'WiFi-Students' VLAN:

Template: debian-edu-router-config/net-int-vlanid-wifiteachers
Type: string
Default: 22
_Description: VLAN-ID for the 'WiFi-Teachers' VLAN:

Template: debian-edu-router-config/net-int-vlanid-wifiguests
Type: string
Default: 24
_Description: VLAN-ID for the 'WiFi-Guests' VLAN:

Template: debian-edu-router-config/net-int-vlanid-printers
Type: string
Default: 40
_Description: VLAN-ID for the 'Printers' VLAN:

# networks as physical NICs on the internal network

Template: debian-edu-router-config/net-connect-int-iface-openlan
Type: error
_Description: Please connect the network cable of the 'OpenLAN' interface, now.

Template: debian-edu-router-config/net-int-iface-openlan
Type: select
Choices: ${choices}
_Description: Physical NIC for the internal 'OpenLAN' network:
 The following network interfaces on this system are connected to a
 network.
 .
 ${extended}

Template: debian-edu-router-config/net-int-iface-openlan-assigned
Type: error
_Description: The internal 'OpenLAN' interface has been successfully assigned.
 Network interface '${iface}' will be used as the host's internal
 'OpenLAN' interface.

Template: debian-edu-router-config/net-connect-int-iface-education
Type: error
_Description: Please connect the network cable of the 'Education' interface, now.

Template: debian-edu-router-config/net-int-iface-education
Type: select
Choices: ${choices}
_Description: Physical NIC for the internal 'Education' network:
 The following network interfaces on this system are connected to a
 network.
 .
 ${extended}

Template: debian-edu-router-config/net-int-iface-education-assigned
Type: error
_Description: The internal 'Education' interface has been successfully assigned.
 Network interface '${iface}' will be used as the host's internal
 'Education' interface.

Template: debian-edu-router-config/net-connect-int-iface-mgmt
Type: error
_Description: Please connect the network cable of the 'Mgmt' interface, now.

Template: debian-edu-router-config/net-int-iface-mgmt
Type: select
Choices: ${choices}
_Description: Physical NIC for the internal 'Mgmt' network:
 The following network interfaces on this system are connected to a
 network.
 .
 ${extended}

Template: debian-edu-router-config/net-int-iface-mgmt-assigned
Type: error
_Description: The internal 'Mgmt' interface has been successfully assigned.
 Network interface '${iface}' will be used as the host's internal 'Mgmt'
 interface.

Template: debian-edu-router-config/net-connect-int-iface-schooladministration
Type: error
_Description: Please connect the cable of the 'School-Administration' interface, now.

Template: debian-edu-router-config/net-int-iface-schooladministration
Type: select
Choices: ${choices}
_Description: Physical NIC for the internal 'School-Administration' network:
 The following network interfaces on this system are connected to a
 network.
 .
 ${extended}

Template: debian-edu-router-config/net-int-iface-schooladministration-assigned
Type: error
_Description: The internal 'School-Administration' NIC has been successfully assigned.
 Network interface '${iface}' will be used as the host's internal
 'School-Administration' interface.

Template: debian-edu-router-config/net-connect-int-iface-wifistudents
Type: error
_Description: Please connect the network cable of the 'WiFi-Students' interface, now.

Template: debian-edu-router-config/net-int-iface-wifistudents
Type: select
Choices: ${choices}
_Description: Physical NIC for the internal 'WiFi-Students' network:
 The following network interfaces on this system are connected to a
 network.
 .
 ${extended}

Template: debian-edu-router-config/net-int-iface-wifistudents-assigned
Type: error
_Description: The internal 'WiFi-Students' interface has been successfully assigned.
 Network interface '${iface}' will be used as the host's internal
 'WiFi-Students' interface.

Template: debian-edu-router-config/net-connect-int-iface-wifiteachers
Type: error
_Description: Please connect the network cable of the 'WiFi-Teachers' interface, now.

Template: debian-edu-router-config/net-int-iface-wifiteachers
Type: select
Choices: ${choices}
_Description: Physical NIC for the internal 'WiFi-Teachers' network:
 The following network interfaces on this system are connected to a
 network.
 .
 ${extended}

Template: debian-edu-router-config/net-int-iface-wifiteachers-assigned
Type: error
_Description: The internal 'WiFi-Teachers' interface has been successfully assigned.
 Network interface '${iface}' will be used as the host's internal
 'WiFi-Teachers' interface.

Template: debian-edu-router-config/net-connect-int-iface-wifiguests
Type: error
_Description: Please connect the network cable of the 'WiFi-Guests' interface, now.

Template: debian-edu-router-config/net-int-iface-wifiguests
Type: select
Choices: ${choices}
_Description: Physical NIC for the internal 'WiFi-Guests' network:
 The following network interfaces on this system are connected to a
 network.
 .
 ${extended}

Template: debian-edu-router-config/net-int-iface-wifiguests-assigned
Type: error
_Description: The internal 'WiFi-Guests' interface has been successfully assigned.
 Network interface '${iface}' will be used as the host's internal
 'WiFi-Guests' interface.

Template: debian-edu-router-config/net-connect-int-iface-printers
Type: error
_Description: Please connect the network cable of the 'Printers' interface, now.

Template: debian-edu-router-config/warn-not-enough-ifaces-available
Type: error
_Description: There are not enough network interfaces available!
 ERROR: You selected ${num_networks} internal network(s),
 ${num_networks_via_vlan} of them use(s) a single VLAN trunk and there are only
 ${num_ifaces} out of ${num_total_ifaces} network interface(s) available.
 Please keep in mind that the Uplink also needs a network interface.
 .
 Here are some suggestions to fix this issue:
 .
 - Connect more network cables/interfaces
 .
 - Deselect some networks,
 .
 - Consider using a VLAN trunk,
 .
 - Use another setup mode (for example step-by-step or offline mode).

Template: debian-edu-router-config/net-int-iface-printers
Type: select
Choices: ${choices}
_Description: Physical NIC for the internal 'Printers' network:
 The following network interfaces on this system are connected to a
 network.
 .
 ${extended}

Template: debian-edu-router-config/net-int-iface-printers-assigned
Type: error
_Description: The internal 'Printers' interface has been successfully assigned.
 Network interface '${iface}' will be used as the host's internal
 'Printers' interface.

Template: debian-edu-router-config/net-ip-versions-enabled
Type: multiselect
Choices: IPv4, IPv6
Default: IPv4
_Description: Which IP versions are to be supported during configuration of the networks?
 .
 Questions regarding a specific IP version won't be asked if its not
 selected here.

Template: debian-edu-router-config/net-networks-staticip-v4
Type: multiselect
Choices: ${choices}
_Description: Networks for which this host will have a static IPv4 address:

Template: debian-edu-router-config/net-networks-staticip-v6
Type: multiselect
Choices: ${choices}
_Description: Networks for which this host will have a static IPv6 address:

Template: debian-edu-router-config/net-networks-dhcpclient-v4
Type: multiselect
Choices: ${choices}
_Description: Networks for which this host's IPv4 address will be configured via DHCPv4:

Template: debian-edu-router-config/net-networks-auto-v6
Type: multiselect
Choices: ${choices}
_Description: Networks for which this host's IPv6 address will be configured via RA:

Template: debian-edu-router-config/net-networks-dhcpclient-v6
Type: multiselect
Choices: ${choices}
_Description: Networks with IPv6 parameters configured via DHCPv6:

# This template will never be queried
Template: debian-edu-router-config/net-networks-manual-v4
Type: multiselect
Choices: ${choices}
_Description: Networks for which this host will have no configured IPv4 address:

# This template will never be queried
Template: debian-edu-router-config/net-networks-manual-v6
Type: multiselect
Choices: ${choices}
_Description: Networks for which this host will have no configured IPv6 address:

Template: debian-edu-router-config/net-syntax-invalid-ipv4
Type: error
_Description: Invalid IPv4 address/netmask syntax. Please retry!
 Make sure you specify IPv4 address _and_ the network's netmask.

Template: debian-edu-router-config/net-syntax-invalid-ipv6
Type: error
_Description: Invalid IPv6 address/prefix syntax. Please retry!
 Make sure you specify IPv6 address _and_ the network's prefix.

Template: debian-edu-router-config/net-ext-address-v4-uplink
Type: string
_Description: IPv4 address/netmask of the external 'Uplink' NIC:
 An IPv4 address is mandatory for the static IP configuration of the
 'Uplink' interface.
 .
 Make sure to use proper IPv4 address/netmask syntax.
 .
 Examples: 192.168.1.1/24, 172.16.0.1/255.255.255.0

Template: debian-edu-router-config/net-ext-address-v6-uplink
Type: string
_Description: IPv6 address/netmask of the external 'Uplink' NIC:
 IPv6 support is optional. Please leave empty if you want to skip static
 IPv6 address configuration on the 'Uplink' interface now and do this
 later.

Template: debian-edu-router-config/net-ext-gateway-v4-uplink
Type: string
_Description: IPv4 address of the 'Uplink' network's gateway:
 When configuring a static IPv4 address on the 'Uplink' network
 interface, it is required to provide an IPv4 gateway address. The
 gateway address needs to be reachable within the 'Uplink' network's IPv4
 subnet.
 .
 Make sure to use proper IPv4 address syntax.
 .
 Examples: 192.168.1.253, 172.16.0.254

Template: debian-edu-router-config/net-ext-gateway-v6-uplink
Type: string
_Description: IPv6 address of the 'Uplink' network's gateway:
 When configuring a static IPv6 address on the 'Uplink' network
 interface, it is required to provide an IPv6 gateway address. The
 gateway address needs to be reachable within the 'Uplink' network's IPv6
 subnet or via the interface's IPv6 link-local address.
 .
 Make sure to use proper IPv6 address syntax.
 .
 Examples: 2001:0425:2ca1::1, fe80::1

# Also used in LDAP/AD connector plugin. Caution while editing!
Template: debian-edu-router-config/net-syntax-invalid-nameserver
Type: error
_Description: Invalid IPv4/IPv6 address syntax in list of DNS servers. Please retry!

Template: debian-edu-router-config/net-ext-nameservers-uplink
Type: string
_Description: Upstream DNS servers:
 You can provide a list of explicit DNS upstream servers to be used by
 this system. If you have configured your 'Uplink' network interface's IP
 address(es) statically, providing at least one DNS server address here
 will be mandatory.
 .
 If you use DHCPv4/DHCPv6 for 'Uplink' network interface configuration,
 you may leave this empty.
 .
 If DNS upstream servers are provided here, they will also be used for
 forwarding DNS queries to from the internal networks if selected so in a
 later configuration query.
 .
 Make sure to use proper IPv4/IPv6 address syntax for DNS server
 addresses. Mutiple IP addresses can be separated by blanks and/or
 commas. Please note that /etc/resolv.conf only supports a maximum of
 three nameserver entries.
 .
 Example: 192.168.1.253, 2001:0425:2ca1::1, fe80::1

Template: debian-edu-router-config/net-ext-nameservers-uplink-required
Type: error
_Description: Static 'Uplink' configuration requires an external nameserver!
 ERROR: When configuring the 'Uplink' network interface statically, at
 least one external nameserver's IPv4 and/or IPv6 address needs to be
 provided.

Template: debian-edu-router-config/net-int-address-v4-openlan
Type: string
_Description: IPv4 address/netmask of the internal 'OpenLAN' NIC:
 An IPv4 address is mandatory for the static IP configuration of the
 'OpenLAN' interface .
 .
 Make sure to use proper IPv4 address/netmask syntax.
 .
 Examples: 192.168.100.253/24, 192.168.100.253/255.255.255.0

Template: debian-edu-router-config/net-int-address-v6-openlan
Type: string
_Description: IPv6 address/netmask of the internal 'OpenLAN' NIC:
 IPv6 support is optional. Please leave empty if you want to skip static
 IPv6 address configuration on the 'OpenLAN' interface now and do this
 later.
 .
 Make sure to use proper IPv6 address/netmask syntax.
 .
 Example: 2345:0425:2ca1::0567:5673:23b5/64

Template: debian-edu-router-config/net-int-address-v4-education
Type: string
_Description: IPv4 address/netmask of the internal 'Education' NIC:
 An IPv4 address is mandatory for the static IP configuration of the
 'Education' interface .
 .
 Make sure to use proper IPv4 address/netmask syntax.
 .
 Examples: 10.0.0.1/8, 10.0.0.1/255.0.0.0

Template: debian-edu-router-config/net-int-address-v6-education
Type: string
_Description: IPv6 address/netmask of the internal 'Education' NIC:
 IPv6 support is optional. Please leave empty if you want to skip static
 IPv6 address configuration on the 'Education' interface now and do this
 later.
 .
 Make sure to use proper IPv6 address/netmask syntax.
 .
 Example: 2345:0425:2ca1::0567:5673:23b5/64

Template: debian-edu-router-config/net-int-address-v4-mgmt
Type: string
_Description: IPv4 address/netmask of the internal 'Mgmt' NIC:
 An IPv4 address is mandatory for the static IP configuration of the
 'Mgmt' interface .
 .
 Make sure to use proper IPv4 address/netmask syntax.
 .
 Examples: 172.16.0.253/24, 172.16.0.253/255.255.255.0

Template: debian-edu-router-config/net-int-address-v6-mgmt
Type: string
_Description: IPv6 address/netmask of the internal 'Mgmt' NIC:
 IPv6 support is optional. Please leave empty if you want to skip static
 IPv6 address configuration on the 'Mgmt' interface now and do this
 later.
 .
 Make sure to use proper IPv6 address/netmask syntax.
 .
 Example: 2345:0425:2ca1::0567:5673:23b5/64

Template: debian-edu-router-config/net-int-address-v4-schooladministration
Type: string
_Description: IPv4 address/netmask of the internal 'School-Administration' NIC:
 An IPv4 address is mandatory for the static IP configuration of the
 'School-Administration' interface .
 .
 Make sure to use proper IPv4 address/netmask syntax.
 .
 Examples: 172.16.8.253/24, 172.16.8.253/255.255.255.0

Template: debian-edu-router-config/net-int-address-v6-schooladministration
Type: string
_Description: IPv6 address/netmask of the internal 'School-Administration' NIC:
 IPv6 support is optional. Please leave empty if you want to skip static
 IPv6 address configuration on the 'School-Administration' interface now
 and do this later.
 .
 Make sure to use proper IPv6 address/netmask syntax.
 .
 Example: 2345:0425:2ca1::0567:5673:23b5/64

Template: debian-edu-router-config/net-int-address-v4-wifistudents
Type: string
_Description: IPv4 address/netmask of the internal 'WiFi-Students' NIC:
 An IPv4 address is mandatory for the static IP configuration of the
 'WiFi-Students' interface .
 .
 Make sure to use proper IPv4 address/netmask syntax.
 .
 Examples: 172.21.0.0/21, 172.21.0.0/255.255.248.0

Template: debian-edu-router-config/net-int-address-v6-wifistudents
Type: string
_Description: IPv6 address/netmask of the internal 'WiFi-Students' NIC:
 IPv6 support is optional. Please leave empty if you want to skip static
 IPv6 address configuration on the 'WiFi-Students' interface now and do
 this later.
 .
 Make sure to use proper IPv6 address/netmask syntax.
 .
 Example: 2345:0425:2ca1::0567:5673:23b5/64

Template: debian-edu-router-config/net-int-address-v4-wifiteachers
Type: string
_Description: IPv4 address/netmask of the internal 'WiFi-Teachers' NIC:
 An IPv4 address is mandatory for the static IP configuration of the
 'WiFi-Teachers' interface .
 .
 Make sure to use proper IPv4 address/netmask syntax.
 .
 Examples: 172.21.8.0/21, 172.21.8.0/255.255.248.0

Template: debian-edu-router-config/net-int-address-v6-wifiteachers
Type: string
_Description: IPv6 address/netmask of the internal 'WiFi-Teachers' NIC:
 IPv6 support is optional. Please leave empty if you want to skip static
 IPv6 address configuration on the 'WiFi-Teachers' interface now and do
 this later.
 .
 Make sure to use proper IPv6 address/netmask syntax.
 .
 Example: 2345:0425:2ca1::0567:5673:23b5/64

Template: debian-edu-router-config/net-int-address-v4-wifiguests
Type: string
_Description: IPv4 address/netmask of the internal 'WiFi-Guests' NIC:
 An IPv4 address is mandatory for the static IP configuration of the
 'WiFi-Guests' interface .
 .
 Make sure to use proper IPv4 address/netmask syntax.
 .
 Examples: 172.21.16.0/21, 172.21.16.0/255.255.248.0

Template: debian-edu-router-config/net-int-address-v6-wifiguests
Type: string
_Description: IPv6 address/netmask of the internal 'WiFi-Guests' NIC:
 IPv6 support is optional. Please leave empty if you want to skip static
 IPv6 address configuration on the 'WiFi-Guests' interface now and do
 this later.
 .
 Make sure to use proper IPv6 address/netmask syntax.
 .
 Example: 2345:0425:2ca1::0567:5673:23b5/64


Template: debian-edu-router-config/net-int-address-v4-printers
Type: string
_Description: IPv4 address/netmask of the internal 'Printers' NIC:
 An IPv4 address is mandatory for the static IP configuration of the
 'Printers' interface .
 .
 Make sure to use proper IPv4 address/netmask syntax.
 .
 Examples: 172.16.1.253/24, 172.16.1.253/255.255.255.0

Template: debian-edu-router-config/net-int-address-v6-printers
Type: string
_Description: IPv6 address/netmask of the internal 'Printers' NIC:
 IPv6 support is optional. Please leave empty if you want to skip static
 IPv6 address configuration on the 'Printers' interface now and do this
 later.
 .
 Make sure to use proper IPv6 address/netmask syntax.
 .
 Example: 2345:0425:2ca1::0567:5673:23b5/64

#
# Service: Firewall
#

Template: debian-edu-router-config/service-firewall-networks-nat
Type: multiselect
Choices: ${choices}
_Description: Networks that are NAT'ed (and hidden) behind the 'Uplink' address:
 Computers on networks selected here will connect to the internet via
 IPv4 and NAT (network address translation). Hosts on the internet will
 only "see" this system's external IPv4 address, whereas individual
 computers on the internal network stay "hidden".

Template: debian-edu-router-config/service-firewall-networks-routed
Type: multiselect
Choices: ${choices}
_Description: Networks that are directly routed into the 'Uplink' network:
 Computers on networks selected here will be directly routed to the
 'Uplink' network via IPv4. Hosts on the 'Uplink' network will "see" each
 individual computer on the internal network.
 .
 Important: When enabling routing of internal networks directly into the
 internet, you must make sure that hosts on the internal networks will
 get assigned publicly addressable IPv4 addresses. Otherwise, you'd need
 another NAT gateway between this system's 'Uplink' network and the
 internet.

# This template will never be queried
Template: debian-edu-router-config/service-firewall-networks-hostonly
Type: multiselect
Choices: ${choices}
_Description: Networks that are host-only, i.e. neither routed nor NAT'ed:

Template: debian-edu-router-config/service-firewall-networks-allow-internet
Type: multiselect
Choices: ${choices}
_Description: Networks that are allowed to access the internet, directly:
 For computers on internal networks you can choose between two default
 policies regarding internet access: all allowed and all blocked.
 .
 Networks selected here will be allowed to access the internet, directly.
 Networks not selected here will be blocked from direct internet access.
 .
 For blocked internal networks, you will later be provided with a
 configuration dialog that allows you to define host based and port based
 exceptions from this default network rule.
 .
 Also if you wish to filter user content you want to disable direct internet
 access in order to assure that no one can bypass the content filter proxy.

# This template will never be queried
Template: debian-edu-router-config/service-firewall-networks-block-internet
Type: multiselect
Choices: ${choices}
_Description: Networks that are blocked from internet access:

Template: debian-edu-router-config/service-firewall-trustworthy-ips
Type: string
_Description: Trustworthy internal IPs and networks:
 Internal addresses/networks which are declared trustworthy can directly
 connect to the internet even if its network is configured to block
 direct internet access.
 .
 The matching of addresses/networks to configured internal networks happens
 automatically.
 .
 You can enter IPv4/IPv6 single addresses or entire networks using the
 address/netmask CIDR syntax.
 .
 Example: 10.0.0.0/8 10.0.2.2 192.168.0.1/24
 2345:0425:2ca1::0567:5673:23b5/64 fdce:4879:a1e9:e351:1a01:be9:4d9a:157d

Template: debian-edu-router-config/service-firewall-invalid-trustworthy-ips
Type: error
_Description: Validation of addresses/networks failed. Please retry!
 The reason validation failed is: ${reason}
 .
 Make sure you only specify IPv4/IPv6 adresses/networks which are part of
 the networks configured.
 .
 For example, you configured the 'Mgmt' network to 172.16.0.253/24. Now
 one can add the 172.16.0.16/28 network as to be allowed to directly
 connect to the internet. Effectively then, all IPv4 addresses from
 172.16.0.16 to 172.16.0.31 will be granted direct internet access.
 .
 This example applies to all configured networks and also to IPv6 (if
 enabled).

Template: debian-edu-router-config/service-firewall-reverse-nat-configs
Type: string
_Description: Reverse NAT configuration:
 Syntax: [tcp:/udp:]<external_port>:<internal_address>[:internal_port]
 .
 Here, you can configure services that shall be reachable from the
 internet (via your 'Uplink' IPv4 address). Multiple configurations are
 possible and should be seperated by spaces.
 .
 As <internal_address> make sure you only specify IPv4 addresses that
 match any of the internal networks' IP subnet configurations. Also, you
 can only reverse-NAT into an internal network if it has been configured
 as a NAT network.
 .
 Providing the protocol (udp/tcp) is optional. If not provided, the setup
 will default to tcp+udp. By prepending either 'udp:' or 'tcp:' one can
 limit the reverse NAT to only one of the protocols.
 .
 Specifying the <internal port> is also optional. It will be
 automatically set to <external_port> if empty. <internal_port> and
 <external_port> may differ.

Template: debian-edu-router-config/service-firewall-invalid-reverse-nat-configs
Type: error
_Description: Validation of your input failed. Please retry!
 Reason for validation failure is: ${reason}
 .
 Make sure you only specify internal IPv4 addresses that match with one
 of the internal networks' IP subnet configurations. Also, you can only
 reverse-NAT into an internal network if it has been configured as a NAT
 network.
 .
 For example, you configured the 'Mgmt' network to 172.16.0.253/24 and
 this network is configured to be hidden behind a NAT. Now imagine you
 have a web service running on a server on 172.16.0.100. One can now make
 that web service reachable from the internet via the 'Uplink' IP address
 using this configuration: 80:172.16.0.100:80 443:172.16.0.100:443
 .
 Also valid but shorter: 80:172.16.0.100 443:172.16.0.100
 .
 IPv6 addresses are *not* supported for reverse NAT configuration.

Template: debian-edu-router-config/service-firewall-ssh-incoming
Type: multiselect
Choices: ${choices}
_Description: Incoming SSH connections:
 Internal/external network interfaces via which it is permitted to log
 into this host via SSH.
 .
 If you are installing this device from remote, please make sure to
 include the Uplink interface into the list of allowed network interfaces.
 .
 NOTE: To fully make incoming SSH connections operational, make sure the
 SSH service is installed and enabled.

#
# Service: DHCP
#

Template: debian-edu-router-config/service-dhcp-networks-v4
Type: multiselect
Choices: ${choices}
_Description: Enable DHCPv4 service on this host for the following networks:
 This host can act as DHCPv4 server for all or some of the enabled
 internal networks. Please select for which internal network the DHCPv4
 service shall be enabled.
 .
 Note for Debian Edu system administrators: Don't enable DHCPv4 on the
 Education network. DHCPv4 is normally provided by Debian Edu's main
 server (aka TJENER).

Template: debian-edu-router-config/service-dhcp-networks-v6
Type: multiselect
Choices: ${choices}
_Description: Enable DHCPv6 service on this host for the following networks:
 This host can act as DHCPv6 network parameter server for all or some of
 the enabled internal networks. Please select for which internal network
 the DHCPv6 service shall be enabled.
 .
 Networks selected here will also be provided with IPv6 router
 advertisement support via this host.

Template: debian-edu-router-config/service-dhcp-range-v4-openlan
Type: string
_Description: DHCPv4 address range on network 'OpenLAN':
 The DHCPv4 service offers IPv4 addresses to requesting DHCPv4 clients.
 Provide a start address, an end address and a lease duration time in the
 format shown in below example.
 .
 Leave empty for disabling support for free DHCPv4 leases.
 .
 Example: 192.168.100.20,192.168.100.252,12h

Template: debian-edu-router-config/service-dhcp-range-v4-education
Type: string
_Description: DHCPv4 address range on network 'Education':
 The DHCPv4 service offers IPv4 addresses to requesting DHCPv4 clients.
 Provide a start address, an end address and a lease duration time in the
 format shown in below example.
 .
 Leave empty for disabling support for free DHCPv4 leases.
 .
 Example: 10.16.0.0,10.31.255.255,12h

Template: debian-edu-router-config/service-dhcp-range-v4-mgmt
Type: string
_Description: DHCPv4 address range on network 'Mgmt':
 The DHCPv4 service offers IPv4 addresses to requesting DHCPv4 clients.
 Provide a start address, an end address and a lease duration time in the
 format shown in below example.
 .
 Leave empty for disabling support for free DHCPv4 leases.
 .
 Example: 172.16.0.20,172.16.0.252,12h

Template: debian-edu-router-config/service-dhcp-range-v4-schooladministration
Type: string
_Description: DHCPv4 address range on network 'School-Administration':
 The DHCPv4 service offers IPv4 addresses to requesting DHCPv4 clients.
 Provide a start address, an end address and a lease duration time in the
 format shown in below example.
 .
 Leave empty for disabling support for free DHCPv4 leases.
 .
 Example: 172.16.8.20,172.16.8.252,12h

Template: debian-edu-router-config/service-dhcp-range-v4-wifistudents
Type: string
_Description: DHCPv4 address range on network 'WiFi-Students':
 The DHCPv4 service offers IPv4 addresses to requesting DHCPv4 clients.
 Provide a start address, an end address and a lease duration time in the
 format shown in below example.
 .
 Leave empty for disabling support for free DHCPv4 leases.
 .
 Example: 172.21.0.2,172.21.7.254,12h

Template: debian-edu-router-config/service-dhcp-range-v4-wifiteachers
Type: string
_Description: DHCPv4 address range on network 'WiFi-Teachers':
 The DHCPv4 service offers IPv4 addresses to requesting DHCPv4 clients.
 Provide a start address, an end address and a lease duration time in the
 format shown in below example.
 .
 Leave empty for disabling support for free DHCPv4 leases.
 .
 Example: 172.21.8.2,172.21.15.254,12h

Template: debian-edu-router-config/service-dhcp-range-v4-wifiguests
Type: string
_Description: DHCPv4 address range on network 'WiFi-Guests':
 The DHCPv4 service offers IPv4 addresses to requesting DHCPv4 clients.
 Provide a start address, an end address and a lease duration time in the
 format shown in below example.
 .
 Leave empty for disabling support for free DHCPv4 leases.
 .
 Example: 172.21.16.2,172.21.23.254,12h

Template: debian-edu-router-config/service-dhcp-range-v4-printers
Type: string
_Description: DHCPv4 address range on network 'Printers':
 The DHCPv4 service offers IPv4 addresses to requesting DHCPv4 clients.
 Provide a start address, an end address and a lease duration time in the
 format shown in below example.
 .
 Leave empty for disabling support for free DHCPv4 leases.
 .
 Example: 172.16.1.21,172.16.1.80,12h

Template: debian-edu-router-config/service-syntax-invalid-dhcp-v4-range
Type: error
_Description: Invalid DHCPv4 lease range. Please retry!
 Make sure you specify an IPv4 address range for DHCPv4 as shown
 in the given example.

Template: debian-edu-router-config/not-implemented-yet
Type: error
_Description: Not implemented, yet.
 WARNING: The requested configuration step has not been implemented, yet.

Template: debian-edu-router-config/purging-warn-remaining-config
Type: error
_Description: Purging remaining Configuration
 All router specific networking and firewall settings will be purged from the
 system.
 .
 Please check possibly remaining configuration files before restarting
 services 'networking', 'uif' and 'dnsmasq'.

#
# SSH
#

Template: debian-edu-router-config/service-ssh-custom-port
Type: string
Default: 22
_Description: Port for incomming SSH connections (empty if disabled):
 The default port for incomming SSH connections is 22.
 If you want to disable the sshd service entirely then enter 0 or just
 leave the input field empty.
 .
 Please make sure you still have a way of accessing this machine, if
 you are about to disable the SSH service.
