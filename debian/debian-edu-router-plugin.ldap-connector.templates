Template: debian-edu-router-plugin.ldap-connector/ldap-connector-enabled
Type: boolean
Default: false
_Description: Enable LDAP Connector plugin?
 The LDAP Connector plugin allows one to provide certain pieces
 of information (such as content filter access restrictions) via LDAP.
 .
 If you enable the LDAP connector, more information needs to be provided
 about your LDAP server and its LDAP data tree.
 .
 If you don't enable the LDAP connector here, all functionality of
 the Router will be based on local configuration files.
 .
 If you disable the LDAP connector and it was previously enabled, all
 information retrieved from LDAP previously will be deleted from this
 system.

Template: debian-edu-router-plugin.ldap-connector/ldap-uri
Type: string
_Description: LDAP server URI:
 Please enter the Uniform Resource Identifier of the LDAP server. The format
 is "ldap://<hostname_or_IP_address>:<port>/". Alternatively, "ldaps://" or
 "ldapi://" can be used. The port number is optional.
 .
 When using an 'ldap' scheme it is recommended to use an IP address to
 avoid failures when domain name services are unavailable.
 .
 If you are using the 'ldaps' scheme, it is recommended to put an entry of the
 LDAP server IP address into /etc/hosts.
 Or simply do not require/check certificates in a later question (not recommended).

Template: debian-edu-router-plugin.ldap-connector/ldap-base
Type: string
_Description: LDAP server search base:
 Please enter the distinguished name of the LDAP search base. Many sites use
 the components of their domain names for this purpose. For example, the
 domain "example.net" would use "dc=example,dc=net" as the distinguished name
 of the search base.

Template: debian-edu-router-plugin.ldap-connector/ldap-user-searchfilter
Type: string
Default: uid=%s
_Description: LDAP search filter for user objects:
 Please enter the LDAP search filter to locate a user's DN. The search
 filter, that retrieves all active and valid user objects from LDAP can
 contain up to 15 occurrences of %s which will be replaced by the
 username. (As in "uid=%s" or "sAMAccountName=%s"). Squid will crash if
 other % values than %s are used, or if there are more than 15 %s
 occurrences.

Template: debian-edu-router-plugin.ldap-connector/ldap-auth-type
Type: select
__Choices: none, simple, SASL
_Description: LDAP authentication to use:
 Please choose what type of authentication the LDAP database should
 require (if any):
 .
  * none: no authentication;
  * simple: simple bind DN and password authentication;
  * SASL: any Simple Authentication and Security Layer mechanism.

Template: debian-edu-router-plugin.ldap-connector/ldap-binddn
Type: string
Default: cn=admin,ou=ldap-access,dc=skole,dc=skolelinux,dc=no
_Description: LDAP database user:
 Please enter the name of the account that will be used to log in to the LDAP
 database. This value should be specified as a DN (distinguished name).

Template: debian-edu-router-plugin.ldap-connector/ldap-bindpw
Type: password
_Description: LDAP user password:
 Please enter the password that will be used to log in to the LDAP database.

Template: debian-edu-router-plugin.ldap-connector/ldap-sasl-mech
Type: select
Choices: auto, LOGIN, PLAIN, NTLM, CRAM-MD5, DIGEST-MD5, SCRAM, GSSAPI, SKEY, OTP, EXTERNAL
_Description: SASL mechanism to use:
 Please choose the SASL mechanism that will be used to authenticate to the LDAP
 database:
 .
  * auto: auto-negotiation;
  * LOGIN: deprecated in favor of PLAIN;
  * PLAIN: simple cleartext password mechanism;
  * NTLM: NT LAN Manager authentication mechanism;
  * CRAM-MD5: challenge-response scheme based on HMAC-MD5;
  * DIGEST-MD5: HTTP Digest compatible challenge-response scheme;
  * SCRAM: salted challenge-response mechanism;
  * GSSAPI: used for Kerberos;
  * SKEY: S/KEY mechanism (obsoleted by OTP);
  * OTP: One Time Password mechanism;
  * EXTERNAL: authentication is implicit in the context.

Template: debian-edu-router-plugin.ldap-connector/ldap-sasl-realm
Type: string
_Description: SASL realm:
 Please enter the SASL realm that will be used to authenticate to the LDAP
 database.
 .
 The realm is appended to authentication and authorization identities.
 .
 For GSSAPI, this can be left blank to use information from the Kerberos
 credentials cache.

Template: debian-edu-router-plugin.ldap-connector/ldap-sasl-authcid
Type: string
_Description: SASL authentication identity:
 Please enter the SASL authentication identity that will be used to authenticate to
 the LDAP database.
 .
 This is the login used in LOGIN, PLAIN, CRAM-MD5, and DIGEST-MD5 mechanisms.

Template: debian-edu-router-plugin.ldap-connector/ldap-sasl-authzid
Type: string
_Description: SASL proxy authorization identity:
 Please enter the proxy authorization identity that will be used to authenticate to
 the LDAP database.
 .
 This is the object in the name of which the LDAP request is done.
 This value should be specified as a DN (distinguished name).

Template: debian-edu-router-plugin.ldap-connector/ldap-sasl-secprops
Type: string
_Description: Cyrus SASL security properties:
 Please enter the Cyrus SASL security properties.
 .
 Allowed values are described in the ldap.conf(5) manual page
 in the SASL OPTIONS section.

Template: debian-edu-router-plugin.ldap-connector/ldap-sasl-krb5-ccname
Type: string
Default: /run/debian-edu-router/plugin.ldap-connector.tkt
_Description: Kerberos credential cache file path:
 Please enter the GSSAPI/Kerberos credential cache file name that will be used.

Template: debian-edu-router-plugin.ldap-connector/ldap-starttls
Type: boolean
_Description: Use StartTLS?
 Please choose whether the connection to the LDAP server should use
 StartTLS to encrypt the connection.

Template: debian-edu-router-plugin.ldap-connector/ldap-reqcert
Type: select
__Choices: never, allow, try, demand
_Description: Check server's SSL certificate:
 When an encrypted connection is used, a server certificate can be requested
 and verified. Please choose whether lookups should be configured to require
 a certificate, and whether certificates should be verified for validity:
 .
  * never: no certificate will be requested or verified;
  * allow: a certificate will be requested, but it is not
           required or verified;
  * try: a certificate will be requested and verified, but if no
         certificate is provided, it is ignored;
  * demand: a certificate will be requested, required, and verified.
 .
 NOTE: If you select 'demand', (or 'try') you should install and trust
 some sort of certificate of the LDAP server to this system. You can do this
 easily in the plugins' loginmenu after this setup ended.
 .
 You can choose to manually set up certificates too.

# Was named ldap-behavior-ca-cert once.
Template: debian-edu-router-plugin.ldap-connector/ldap-install-cert-type
Type: select
__Choices: ca-cert, ee-cert, manually
Default: manually
_Description: Certificate variant to be installed:
  This host has been configured to request and verify SSL certificates'
  validity. Please select how, or if, this router should obtain the root
  (or intermediary) CA certificate.
  .
  Installing a CA (certificate authority) certificate on the system, makes the
  EE (end entity) certificate trustworthy. The connection will be secure and
  trusted, even if the EE cert gets (often automatically) replaced.
  This, of course, assumes that the EE cert is signed by the CA cert.
  EE certificates are often short-lived. They intentionally last for a few
  days/months. CA certificates are often long-lived. They intentionally last for
  a few years. This means that this router can bond with the LDAP server once
  and it will work for a few years. This is why using a certificate trust chain
  is strongly recommended.
  .
  'ca-cert':
  .
  This router will try to obtain a root or intermediary CA
  certificate. This option should be selected, when in doubt.
  .
  'ee-cert':
  .
  This router *WON'T* try to obtain a CA certificate. The current EE
  (end-entity) certificate served by your LDAP server, will be installed and
  trusted automatically, instead. You will have to manually keep it updated,
  using the loginmenu, when the certificate expires.
  You won't get a notification or something, the LDAP connection will just
  break some day. Automation of this process is highly discouraged for security
  reasons. A patient attacker could use this to start an MitM attack.
  .
  'manually':
  .
  This router *WON'T* try to obtain a CA or EE certificate. You know exactly
  what you are doing and you want to manually maintain a CA or EE certificate.
  .
  NOTE: Choosing 'ca-cert' or 'ee-cert' will result in a connection attempt to
  another host on your local network. To avoid this, please choose 'manually',
  here.

Template: debian-edu-router-plugin.ldap-connector/ldap-cacertfile
Type: string
Default: /etc/ssl/certs/ca-certificates.crt
_Description: CA certificate file or CA collection file path:
 When certificate checking is enabled this file contains the X.509
 certificate that is used to check the certificate provided by the server.
 .
 On Debian systems, the 'ca-certificates' package keeps this file (default
 value) maintained automatically.
 .
 Only change this path, if you really want to differ from Debian defaults
 (by manually maintaining your own certificate file for example).

Template: debian-edu-router-plugin.ldap-connector/ldap-dns-servers
Type: string
Default: 10.0.2.2
_Description: DNS servers to be used for resolving client hostnames:
 To resolve hostnames from Proxy*Client group objects within the LDAP
 tree, one or multiple DNS servers need to be queried.
 .
 This is usually the AD domain controller or, in Debian Edu, the Tjener server
 (10.0.2.2).
 .
 NOTE: If you have already entered the addresses of your LDAP's DNS servers in
 the Content filter plugin, then you do not need to enter it again here.

Template: debian-edu-router-plugin.ldap-connector/ldap-refresh-filterlists
Type: boolean
Default: false
_Description: Periodically auto-refresh filterlists from LDAP?
 If you enable filterlists auto-refreshing here the router will periodically
 query client and user filterlist policies from LDAP and store them in
 '/var/lib/debian-edu-router/filterlists.d/*.ldap'.
 .
 All Squid instances are reloaded automatically, you don't need to do anything
 manually. The default interval is set to be every 5 minutes.
 .
 You should consider using event-based (instead of interval-based) refreshing
 of filterlists using `systemctl start squid_d-e-r_refresh-ldap-filterlists`
 (or the `debian-edu-router_refresh-ldap-filterlists` command, if systemd isn't
 available.) For Debian Edu, you can utilize GOsa² hook-scripts to achieve
 this.

#
# Proxy*{Client, User}: LDAP GROUP TYPES
#
Template: debian-edu-router-plugin.ldap-connector/ldap-groups-type-nisNetgroup
Type: multiselect
Choices: ${choices}
_Description: Proxy groups that are of group type 'nisNetgroup' in LDAP:
 nisNetgroup: nisNetgroupTriple format: (CLIENT, USER, DOMAIN).
 .
 For Debian Edu, nisNetgroups are recommended as the proxy groups come
 pre-created and pre-configured for Tjener with Debian Edu 13+.
 .
 If you have trouble choosing this, or if you have any other question, please
 take a look at the documentation files at /usr/share/doc/debian-edu-router*

Template: debian-edu-router-plugin.ldap-connector/ldap-groups-type-group
Type: multiselect
Choices: ${choices}
_Description: Proxy groups that are of group type 'group' (Microsoft AD) in LDAP:
 group: Must provide a list of member DNs via the 'member' attribute.
 Member objects referencing computer clients must have the 'dNSHostName'
 attribute set. Member objects referencing persons must have the 'sAMAccount'
 attribute set.
 .
 If you have trouble choosing this, or if you have any other question, please
 take a look at the documentation files at /usr/share/doc/debian-edu-router*

Template: debian-edu-router-plugin.ldap-connector/ldap-groups-type-groupOfNames
Type: multiselect
Choices: ${choices}
_Description: Proxy groups that are of group type 'groupOfNames' in LDAP:
 groupOfNames: Must provide a list of member DNs via the 'member' attribute.
 Member objects referencing computer clients must have the 'dhcpHost'
 attribute set. Member objects referencing persons must have the 'uid'
 attribute set.
 .
 See https://datatracker.ietf.org/doc/html/rfc4519.
 .
 If you have trouble choosing this, or if you have any other question, please
 take a look at the documentation files at /usr/share/doc/debian-edu-router*

Template: debian-edu-router-plugin.ldap-connector/ldap-groups-type-posixGroup
Type: multiselect
Choices: ${choices}
_Description: User proxy groups that are of group type 'posixGroup' in LDAP:
 posixGroup: Must provide a list of UIDs via attribute 'memberUid'.
 .
 See https://datatracker.ietf.org/doc/html/rfc2307.
 .
 If you have trouble choosing this, or if you have any other question, please
 take a look at the documentation files at /usr/share/doc/debian-edu-router*

#
# ERROR MESSAGES
#
Template: debian-edu-router-plugin.ldap-connector/ldap-group-name-empty
Type: error
_Description: Group name cannot be empty.
 Empty LDAP group names are not allowed.
 .
 Please go back using the ESCAPE key and deselect the group instead.

Template: debian-edu-router-plugin.ldap-connector/ldap-group-base-dn-empty
Type: error
_Description: Group base/search DN cannot be empty.
 Empty base/search DNs (Distinguished Names) are not allowed.
 .
 Please go back using the ESCAPE key and deselect the group instead.

Template: debian-edu-router-plugin.ldap-connector/ldap-unconfigured-proxy-groups
Type: error
_Description: There are unconfigured/unselected proxy groups left.
 Please note that there are the following proxy groups left unselected:
 .
 ${groups_left_unselected}
 .
 This means that they'll be ignored and *no* data will be read from LDAP for
 these proxy groups.
 .
 You can go back using the ESCAPE key and select a type for the proxy groups,
 if you wish.

#
# GROUP NAMES: Clients (used for nisNetgroup, group and groupOfNames)
#
Template: debian-edu-router-plugin.ldap-connector/ldap-group-name-ProxyTrustedClient
Type: string
Default: proxy-trusted-client
_Description: Name of the group that maps to ProxyTrustedClients:
 Please make sure that the LDAP group is actually of type ${group_type} in your
 LDAP tree.
 .
 Documentation files: /usr/share/doc/debian-edu-router*.

Template: debian-edu-router-plugin.ldap-connector/ldap-group-name-ProxyAllowClient
Type: string
Default: proxy-allow-client
_Description: Name of the group that maps to ProxyAllowClients:
 Please make sure that the LDAP group is actually of type ${group_type} in your
 LDAP tree.
 .
 Documentation files: /usr/share/doc/debian-edu-router*.

Template: debian-edu-router-plugin.ldap-connector/ldap-group-name-ProxyDenyClient
Type: string
Default: proxy-deny-client
_Description: Name of the group that maps to ProxyDenyClients:
 Please make sure that the LDAP group is actually of type ${group_type} in your
 LDAP tree.
 .
 Documentation files: /usr/share/doc/debian-edu-router*.

Template: debian-edu-router-plugin.ldap-connector/ldap-group-name-ProxyBlacklistClient
Type: string
Default: proxy-blacklist-client
_Description: Name of the group that maps to ProxyBlacklistClients:
 Please make sure that the LDAP group is actually of type ${group_type} in your
 LDAP tree.
 .
 Documentation files: /usr/share/doc/debian-edu-router*.

Template: debian-edu-router-plugin.ldap-connector/ldap-group-name-ProxyWhitelistClient
Type: string
Default: proxy-whitelist-client
_Description: Name of the group that maps to ProxyWhitelistClients:
 Please make sure that the LDAP group is actually of type ${group_type} in your
 LDAP tree.
 .
 Documentation files: /usr/share/doc/debian-edu-router*.

Template: debian-edu-router-plugin.ldap-connector/ldap-group-name-ProxyNoauthClient
Type: string
Default: proxy-tablet-client
_Description: Name of the group that maps to ProxyNoauthClients:
 Please make sure that the LDAP group is actually of type ${group_type} in your
 LDAP tree.
 .
 Documentation files: /usr/share/doc/debian-edu-router*.

#
# GROUP NAMES: Users (used for nisNetgroup, posixGroup, group and groupOfNames)
#
Template: debian-edu-router-plugin.ldap-connector/ldap-group-name-ProxyTrustedUser
Type: string
Default: proxy-trusted-user
_Description: Name of the group that maps to ProxyTrustedUsers:
 Please make sure that the LDAP group is actually of type ${group_type} in your
 LDAP tree.
 .
 Documentation files: /usr/share/doc/debian-edu-router*.

Template: debian-edu-router-plugin.ldap-connector/ldap-group-name-ProxyAllowUser
Type: string
Default: proxy-allow-user
_Description: Name of the group that maps to ProxyAllowUsers:
 Please make sure that the LDAP group is actually of type ${group_type} in your
 LDAP tree.
 .
 Documentation files: /usr/share/doc/debian-edu-router*.

Template: debian-edu-router-plugin.ldap-connector/ldap-group-name-ProxyDenyUser
Type: string
Default: proxy-deny-user
_Description: Name of the group that maps to ProxyDenyUsers:
 Please make sure that the LDAP group is actually of type ${group_type} in your
 LDAP tree.
 .
 Documentation files: /usr/share/doc/debian-edu-router*.

Template: debian-edu-router-plugin.ldap-connector/ldap-group-name-ProxyBlacklistUser
Type: string
Default: proxy-blacklist-user
_Description: Name of the group that maps to ProxyBlacklistUsers:
 Please make sure that the LDAP group is actually of type ${group_type} in your
 LDAP tree.
 .
 Documentation files: /usr/share/doc/debian-edu-router*.

Template: debian-edu-router-plugin.ldap-connector/ldap-group-name-ProxyWhitelistUser
Type: string
Default: proxy-whitelist-user
_Description: Name of the group that maps to ProxyWhitelistUsers:
 Please make sure that the LDAP group is actually of type ${group_type} in your
 LDAP tree.
 .
 Documentation files: /usr/share/doc/debian-edu-router*.

Template: debian-edu-router-plugin.ldap-connector/ldap-groups-search-via-base-dn
Type: multiselect
Choices: ${choices}
_Description: Proxy groups that should be populated via special LDAP search DNs
 Client or user objects that are not organized using groups (nisNetgroup,
 posixGroup, group, groupOfNames...) and that are stored within an LDAP
 subtree can be searched in LDAP via special search DNs.
 .
 For example: Select 'ProxyAllowClient' here, then enter the base DN like
 'OU=Teachers,dc=skole,dc=skolelinux,dc=no' in a following question.
 .
 This will have the effect that all valid client computer objects within the
 OU=Teachers,dc=skole,dc=skolelinux,dc=no subtree will be loaded into the
 ProxyAllowClient group. This enables unrestricted internet access to all
 teacher computers automatically.
 .
 Specifying a search DN can be used in addition to using groups.

#
# GROUPS BASE DN: Clients (used for Search-via-BaseDN only)
#
Template: debian-edu-router-plugin.ldap-connector/ldap-group-base-dn-ProxyTrustedClient
Type: string
Default: OU=ProxyTrustedClient,dc=skole,dc=skolelinux,dc=no
_Description: LDAP subtree DN where you want ProxyTrustedClients to be searched in:
 Documentation files: /usr/share/doc/debian-edu-router*.

Template: debian-edu-router-plugin.ldap-connector/ldap-group-base-dn-ProxyAllowClient
Type: string
Default: OU=ProxyAllowClient,dc=skole,dc=skolelinux,dc=no
_Description: LDAP subtree DN where you want ProxyAllowClients to be searched in:
 Documentation files: /usr/share/doc/debian-edu-router*.

Template: debian-edu-router-plugin.ldap-connector/ldap-group-base-dn-ProxyDenyClient
Type: string
Default: OU=ProxyDenyClient,dc=skole,dc=skolelinux,dc=no
_Description: LDAP subtree DN where you want ProxyDenyClients to be searched in:
 Documentation files: /usr/share/doc/debian-edu-router*.

Template: debian-edu-router-plugin.ldap-connector/ldap-group-base-dn-ProxyBlacklistClient
Type: string
Default: OU=ProxyBlacklistClient,dc=skole,dc=skolelinux,dc=no
_Description: LDAP subtree DN where you want ProxyBlacklistClients to be searched in:
 Documentation files: /usr/share/doc/debian-edu-router*.

Template: debian-edu-router-plugin.ldap-connector/ldap-group-base-dn-ProxyWhitelistClient
Type: string
Default: OU=ProxyWhitelistClient,dc=skole,dc=skolelinux,dc=no
_Description: LDAP subtree DN where you want ProxyWhitelistClients to be searched in:
 Documentation files: /usr/share/doc/debian-edu-router*.

Template: debian-edu-router-plugin.ldap-connector/ldap-group-base-dn-ProxyNoauthClient
Type: string
Default: OU=ProxyNoauthClient,dc=skole,dc=skolelinux,dc=no
_Description: LDAP subtree DN where you want ProxyNoauthClients to be searched in:
 Documentation files: /usr/share/doc/debian-edu-router*.

#
# GROUPS BASE DN: Users (used for Search-via-BaseDN only)
#
Template: debian-edu-router-plugin.ldap-connector/ldap-group-base-dn-ProxyTrustedUser
Type: string
Default: OU=ProxyTrustedUser,dc=skole,dc=skolelinux,dc=no
_Description: LDAP subtree DN where you want ProxyTrustedUsers to be searched in:
 Documentation files: /usr/share/doc/debian-edu-router*.

Template: debian-edu-router-plugin.ldap-connector/ldap-group-base-dn-ProxyAllowUser
Type: string
Default: OU=ProxyAllowUser,dc=skole,dc=skolelinux,dc=no
_Description: LDAP subtree DN where you want ProxyAllowUsers to be searched in:
 Documentation files: /usr/share/doc/debian-edu-router*.

Template: debian-edu-router-plugin.ldap-connector/ldap-group-base-dn-ProxyDenyUser
Type: string
Default: OU=ProxyDenyUser,dc=skole,dc=skolelinux,dc=no
_Description: LDAP subtree DN where you want ProxyDenyUsers to be searched in:
 Documentation files: /usr/share/doc/debian-edu-router*.

Template: debian-edu-router-plugin.ldap-connector/ldap-group-base-dn-ProxyBlacklistUser
Type: string
Default: OU=ProxyBlacklistUser,dc=skole,dc=skolelinux,dc=no
_Description: LDAP subtree DN where you want ProxyBlacklistUsers to be searched in:
 Documentation files: /usr/share/doc/debian-edu-router*.

Template: debian-edu-router-plugin.ldap-connector/ldap-group-base-dn-ProxyWhitelistUser
Type: string
Default: OU=ProxyWhitelistUser,dc=skole,dc=skolelinux,dc=no
_Description: LDAP subtree DN where you want ProxyWhitelistUsers to be searched in:
 Documentation files: /usr/share/doc/debian-edu-router*.
