Source: debian-edu-router
Section: admin
Priority: optional
Maintainer: Debian Edu Developers <debian-edu@lists.debian.org>
Uploaders: Mike Gabriel <sunweaver@debian.org>
Build-Depends: debhelper-compat (= 13),
Rules-Requires-Root: no
Standards-Version: 4.7.2
Homepage: https://wiki.debian.org/DebianEdu/Router
Vcs-Browser: https://salsa.debian.org/debian-edu/debian-edu-router
Vcs-Git: https://salsa.debian.org/debian-edu/debian-edu-router.git

Package: debian-edu-router-config
Architecture: all
Multi-Arch: foreign
Pre-Depends: ${misc:Pre-Depends},
             debconf,
             debian-edu-router-common (>= ${source:Version}),
             debian-edu-router-common (<< ${source:Version}.1~),
Depends: ${misc:Depends},
         dnsmasq,
         ifupdown,
         iptraf,
         iproute2,
         netcat-openbsd,
         openssh-client,
         openssh-server,
         procps,
         uif (>= 1.99.0-3~),
         vlan,
Recommends: gpm,
            ipcalc,
            ipv6calc,
            htop,
Suggests: debian-edu-router-plugin.mdns-reflector,
          debian-edu-router-plugin.content-filter,
          etckeeper,
Conflicts: connman,
           firehol,
           ifupdown2,
           netplan.io,
           network-manager,
           shorewall,
           ufw,
Description: Debian Edu Router Configuration
 The Debian Edu Router project provides installation routines for
 setting up a network router for the Debian Edu system (or for other
 school networks).
 .
 Debian Edu Router uses FAI (Fully Automatic Installation) to install and
 configure the router system.
 .
 Debian Edu Router knows how to interact with the following school IT
 solutions:
 .
   - Debian Edu / Skolelinux
   - MNS+ (common solution of the German state of Rhineland-Palatinate)
 .
 This package is to be installed on the Debian Edu Router system. It provides
 dependencies, configuration files, and scripts for interacting with
 well-known school IT systems.

Package: debian-edu-router-common
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
Breaks: debian-edu-router-config (<< 2.13.0~exp2~),
Replaces: debian-edu-router-config (<< 2.13.0~exp2~),
Description: Debian Edu Router - common/shared files
 The Debian Edu Router project provides installation routines for
 setting up a network router for the Debian Edu system (or for other
 school networks).
 .
 Debian Edu Router uses FAI (Fully Automatic Installation) to install and
 configure the router system.
 .
 Debian Edu Router knows how to interact with the following school IT
 solutions:
 .
   - Debian Edu / Skolelinux
   - MNS+ (common solution of the German state of Rhineland-Palatinate)
 .
 This package contains files shared by several of the other Debian Edu Router
 related packages. Esp. -config and -plugin.* packages need to pre-depend
 on this package due to internal script dependencies.

Package: debian-edu-router-fai
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
         fai-server,
         ucf,
Recommends: binutils,
            dialog,
            squashfs-tools,
            xorriso,
Pre-Depends: ${misc:Pre-Depends},
             debian-edu-router-common (>= ${source:Version}),
             debian-edu-router-common (<< ${source:Version}.1~),
Suggests: debian-edu-router-fai-l10n,
Breaks: debian-edu-router,
Replaces: debian-edu-router,
Description: FAI config space (et al.) for the Debian Edu Router system
 The Debian Edu Router project provides installation routines for
 setting up a network router for the Debian Edu system (or for other
 school networks).
 .
 Debian Edu Router uses FAI (Fully Automatic Installation) to install and
 configure the router system.
 .
 Debian Edu Router knows how to interact with the following school IT
 solutions:
 .
   - Debian Edu / Skolelinux
   - MNS+ (common solution of the German state of Rhineland-Palatinate)
 .
 This package contains the FAI config space, installation routines for
 deploying Debian Edu Router system over the http(s):// protocol.

Package: debian-edu-router-fai-l10n
Section: localization
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
Breaks: debian-edu-router-fai (<< 2.13.0~),
Replaces: debian-edu-router-fai (<< 2.13.0~),
Description: FAI config space locale files for the Debian Edu Router system
 The Debian Edu Router project provides installation routines for
 setting up a network router for the Debian Edu system (or for other
 school networks).
 .
 Debian Edu Router uses FAI (Fully Automatic Installation) to install and
 configure the router system.
 .
 Debian Edu Router knows how to interact with the following school IT
 solutions:
 .
   - Debian Edu / Skolelinux
   - MNS+ (common solution of the German state of Rhineland-Palatinate)
 .
 This package contains locale files (language translations of installer
 dialogs) for the FAI config space.

Package: debian-edu-router-deployserver
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
         apache2 | httpd,
Description: Deployment server for the Debian Edu Router system
 The Debian Edu Router project provides installation routines for
 setting up a network router for the Debian Edu system (or for other
 school networks).
 .
 Debian Edu Router uses FAI (Fully Automatic Installation) to install and
 configure the router system.
 .
 Debian Edu Router knows how to interact with the following school IT
 solutions:
 .
   - Debian Edu / Skolelinux
   - MNS+ (common solution of the German state of Rhineland-Palatinate)
 .
 This package contains a deployment server setup that one can use to
 deploy multiple Debian Edu Router systems to various school sites.

Package: debian-edu-router-plugin.mdns-reflector
Architecture: all
Multi-Arch: foreign
Pre-Depends: ${misc:Pre-Depends},
             debconf,
             debian-edu-router-common (>= ${source:Version}),
             debian-edu-router-common (<< ${source:Version}.1~),
Depends: ${misc:Depends},
         debian-edu-router-config (>= ${source:Version}~),
         debian-edu-router-config (<< ${source:Version}.1~),
         mdns-reflector,
Enhances: debian-edu-router-config
Description: Debian Edu Router Plugin for mDNS reflection
 The Debian Edu Router project provides installation routines for
 setting up a network router for the Debian Edu system (or for other
 school networks).
 .
 Debian Edu Router uses FAI (Fully Automatic Installation) to install and
 configure the router system.
 .
 Debian Edu Router knows how to interact with the following school IT
 solutions:
 .
   - Debian Edu / Skolelinux
   - MNS+ (common solution of the German state of Rhineland-Palatinate)
 .
 This package adds an mDNS reflector service to Debian Edu Router. Services
 can get announced via DNS-SD over multicast DNS (Bonjour). But multicast
 packages are designed to not be routed. One way to still announce services
 across network boundaries is to use an mDNS package reflector. It'll
 distribute services to selected internal networks configured in
 debian-edu-router-config.

Package: debian-edu-router-plugin.content-filter
Architecture: all
Multi-Arch: foreign
Pre-Depends: ${misc:Pre-Depends},
             debconf,
             debian-edu-router-common (>= ${source:Version}),
             debian-edu-router-common (<< ${source:Version}.1~),
             ssl-cert,
Depends: ${misc:Depends},
         apache2,
         debian-edu-router-config (>= ${source:Version}~),
         debian-edu-router-config (<< ${source:Version}.1~),
         dnsutils,
         e2guardian (>= 5.5.4),
         squid-openssl,
         systemd,
Recommends:
         squid-langpack,
Enhances: debian-edu-router-config
Description: Debian Edu Router Plugin for content filtering
 The Debian Edu Router project provides installation routines for
 setting up a network router for the Debian Edu system (or for other
 school networks).
 .
 Debian Edu Router uses FAI (Fully Automatic Installation) to install and
 configure the router system.
 .
 Debian Edu Router knows how to interact with the following school IT
 solutions:
 .
   - Debian Edu / Skolelinux
   - MNS+ (common solution of the German state of Rhineland-Palatinate)
 .
 This package adds an squid/e2guardian based content filtering system.

Package: debian-edu-router-plugin.ldap-connector
Architecture: all
Multi-Arch: foreign
Pre-Depends: ${misc:Pre-Depends},
             debconf,
             debian-edu-router-common (>= ${source:Version}),
             debian-edu-router-common (<< ${source:Version}.1~),
Depends: ${misc:Depends},
         debian-edu-router-config (>= ${source:Version}~),
         debian-edu-router-config (<< ${source:Version}.1~),
         ldap-utils,
         systemd,
Recommends: debian-edu-router-plugin.content-filter
Enhances: debian-edu-router-config
Description: Debian Edu Router Plugin for LDAP/AD connector
 The Debian Edu Router project provides installation routines for
 setting up a network router for the Debian Edu system (or for other
 school networks).
 .
 Debian Edu Router uses FAI (Fully Automatic Installation) to install and
 configure the router system.
 .
 Debian Edu Router knows how to interact with the following school IT
 solutions:
 .
   - Debian Edu / Skolelinux
   - MNS+ (common solution of the German state of Rhineland-Palatinate)
 .
 This package adds an LDAP/AD connector to obtain configuration information
 and content filtering list settings from a site's LDAP database. This means
 this plugin is only fully functional in addition to Debian Edu Router Plugin:
 Content filter.

Package: debian-edu-router-plugin.krb5-connector
Architecture: all
Multi-Arch: foreign
Pre-Depends: ${misc:Pre-Depends},
             debconf,
             debian-edu-router-common (>= ${source:Version}),
             debian-edu-router-common (<< ${source:Version}.1~),
Depends: ${misc:Depends},
         debian-edu-router-config (>= ${source:Version}~),
         debian-edu-router-config (<< ${source:Version}.1~),
         krb5-user,
         msktutil,
Recommends: debian-edu-router-plugin.content-filter
Enhances: debian-edu-router-config
Description: Debian Edu Router Plugin for Kerberos connector
 The Debian Edu Router project provides installation routines for
 setting up a network router for the Debian Edu system (or for other
 school networks).
 .
 Debian Edu Router uses FAI (Fully Automatic Installation) to install and
 configure the router system.
 .
 Debian Edu Router knows how to interact with the following school IT
 solutions:
 .
   - Debian Edu / Skolelinux
   - MNS+ (common solution of the German state of Rhineland-Palatinate)
 .
 This package adds the possibility to let users authenticate using Kerberos5.
 This means this plugin is only functional in addition to Debian Edu Router
 Plugin: Content filter.
