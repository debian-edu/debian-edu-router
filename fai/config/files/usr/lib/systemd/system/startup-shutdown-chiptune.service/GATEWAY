#
# Copyright (C) 2023 Daniel Teichmann <daniel.teichmann@das-netzwerkteam.de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

# Nice chiptune sound when successfully booted and when shutting down.
# Composer: Daniel Teichmann <daniel.teichmann@das-netzwerkteam.de>

[Unit]
Description=Plays chiptune sound at startup and shutdown of the system.
After=networking.service

[Service]
Type=oneshot
RemainAfterExit=true
ExecStart=/bin/bash -c 'A=440; D=587.3295; G=783.9909; A2=880; /usr/bin/beep -f $D -n -f $A -n -f $D -n -f $G -f $A2 -n -f $G -n -f $A2 -l 300'
ExecStop=/bin/bash -c 'C=261.6256; A=440; D=587.3295; A2=880; /usr/bin/beep -f $A2 -n -f $D -n -f $A -n -f $C -l 400'

[Install]
WantedBy=multi-user.target
WantedBy=network-online.target
