#! /bin/bash
#
#   Detect all available NICs
#

# Copyright (C) 2011-2018 Andreas B. Mundt <andi@debian.org>
# Copyright (C) 2022-2023 Pädagogisches Landesinstitut Rheinland-Pfalz
# Copyright (C) 2022-2023 Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

## Variables containing available network interfaces:

N=0
fields="ID_NET_NAME_FROM_DATABASE ID_NET_NAME_ONBOARD ID_NET_NAME_SLOT ID_NET_NAME_PATH"
for NIC in $(ip link show | grep -E "^\w+:" | cut -d ":" -f2 | grep -v lo) ; do
	for field in $fields; do
		name=$(udevadm info /sys/class/net/$NIC | sed -rn "s/^E: $field=(.+)/\1/p")
		if [[ $name ]]; then
			echo "NIC_LABEL${N}=$name" >> $LOGDIR/additional.var
			N=$((N+1))
			break
		fi
	done
done
