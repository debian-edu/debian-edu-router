Default: d-e-r-full::en_US

Name: d-e-r-full::en_US
Description: Debian Edu Router Full - Englisch
Short: Debian Edu Router incl. plugins with English keyboard layout
Long: This installs the Debian Edu Router system with an English (en_US) keyboard layout and all available plugins.
Classes: INSTALL FAIBASE DEBIAN DEBIAN_EDU_ROUTER DEBIAN_EDU_ROUTER_PLUGINS LVM_EDU_ROUTER GATEWAY FIREWALL DNS_SERVER NTP_SERVER

Name: d-e-r-full::de_DE
Description: Debian Edu Router Voll - Deutsch
Short: Debian Edu Router inkl. Plugins mit deutschem Tastaturlayout
Long: Dies installiert das Debian Edu Router System mit deutschem (de_DE) Tastaturlayout und allen verfügbaren Plugins.
Classes: INSTALL FAIBASE DEBIAN DEBIAN_EDU_ROUTER DEBIAN_EDU_ROUTER_PLUGINS LVM_EDU_ROUTER GATEWAY FIREWALL DNS_SERVER NTP_SERVER LANG_GERMAN


Name: d-e-r-min::en_US
Description: Debian Edu Router Minimal - Englisch
Short: Debian Edu Router with English keyboard layout
Long: This installs the Debian Edu Router base system with an English (en_US) keyboard layout.
Classes: INSTALL FAIBASE DEBIAN DEBIAN_EDU_ROUTER LVM_EDU_ROUTER GATEWAY FIREWALL DNS_SERVER NTP_SERVER

Name: d-e-r-min::de_DE
Description: Debian Edu Router Minimal - Deutsch
Short: Debian Edu Router mit deutschem Tastaturlayout
Long: Dies installiert das Debian Edu Router Grundsystem mit deutschem (de_DE) Tastaturlayout.
Classes: INSTALL FAIBASE DEBIAN DEBIAN_EDU_ROUTER LVM_EDU_ROUTER GATEWAY FIREWALL DNS_SERVER NTP_SERVER LANG_GERMAN
