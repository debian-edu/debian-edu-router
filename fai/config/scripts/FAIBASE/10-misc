#! /bin/bash

# Copyright (C) 2001-2012 Thomas Lange <lange@informatik.uni-koeln.de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

error=0; trap 'error=$(($?>$error?$?:$error))' ERR # save maximum error code

echo "$TIMEZONE"    > "$target/etc/timezone"
if [ -L "$target/etc/localtime" ]; then
    ln -sf "/usr/share/zoneinfo/${TIMEZONE}" "$target/etc/localtime"
else
    cp -f "/usr/share/zoneinfo/${TIMEZONE}" "$target/etc/localtime"
fi

if [ -n "$IPADDR" ]; then
    ifclass DHCPC || ainsl -s /etc/hosts "$IPADDR $HOSTNAME.$DOMAIN $HOSTNAME"
else
    ifclass DHCPC && ainsl -s /etc/hosts "127.0.0.1 $HOSTNAME"
fi

if [ -f "$target/etc/hosts.orig" ]; then
    mv "$target/etc/hosts.orig" "$target/etc/hosts"
fi
fcopy -iM /etc/hosts /etc/motd

# make /root accessible only by root
chmod -c 0700 "$target/root"
chown -c root:root "$target/root"
# copy default dotfiles for root account
fcopy -ir /root

# use tmpfs for /tmp if not provided otherwise
if ! grep -qE ".*\s/tmp\s.*" < $target/etc/fstab; then
	ainsl /etc/fstab "tmpfs     /tmp tmpfs     nodev,nosuid,size=50%,mode=1777   0    0"
fi
chmod -c 1777 "${target}/tmp"
chown -c 0:0  "${target}/tmp"

exit $error
