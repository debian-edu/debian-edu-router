#!/bin/bash

set -e

# Copyright (C) 2022-2023 Pädagogisches Landesinstitut Rheinland-Pfalz
# Copyright (C) 2022-2023 Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

if [ "$FAI_ACTION" != "install" ] && [ "$CONVERT" != "true" ] ; then
    exit 0
fi

function do_d-e-r_preseed() {
	pkg="debian-edu-router-config"
	template="$1"
	type="$2"
	value="$3"
	echo $pkg $template $type "$value" | ${ROOTCMD} debconf-set-selections
}

# $1: debconfig question (the items which should be filtered through)
# $2: item to filter out
function filter_multiselect_item() {
	RET=`${ROOTCMD} debconf-get-selections | grep "$1" | head -n1 | cut -f4 -`
	if [ -n "${RET}" ]; then
		echo `echo "${RET}" | sed -Ee "s/$2(,\s*|\$)//g" -e "s/,\s*\$//g"`
	fi
}

# $1: debconfig question (the items which should stay)
# $2: item to be added to debconf question
function add_multiselect_item() {
	RET=`${ROOTCMD} debconf-get-selections | grep "$1" | head -n1 | cut -f4 -`

	# First filter out iface $2 and then just readd it.
	tmp_items=`echo "${RET}" | sed -Ee "s/$2(,\s*|\$)//g" -e "s/,\s*\$//g"`
	if [ -n "${tmp_items}" ]; then
		echo `echo "$2, ${tmp_items}"`
	else
		echo `echo "$2"`
	fi
}

# generate loopback device
cat > "${target}/etc/network/interfaces" <<EOF
###
### Debian Edu Router Network Setup
###

source /etc/network/interfaces.d/*

# The loopback network interface

auto lo
iface lo inet loopback
      dns-search intern
EOF

# Detect our default route (hinting to the uplink NIC)
uplink_iface=$(ip -4 route | grep -E "^default via" | sed -e "s/.*\ dev\ //" | awk '{print $1}')

fields="ID_NET_NAME_FROM_DATABASE ID_NET_NAME_ONBOARD ID_NET_NAME_SLOT ID_NET_NAME_PATH"
for field in ${fields}; do
	name="`udevadm info /sys/class/net/${uplink_iface} | sed -rn "s/^E: $field=(.+)/\1/p" | sed -s 's/\s+//g'`"
	if [[ "${name}" ]]; then
		uplink_iface="${name}"
		break
	fi
done

# auto-detected uplink NIC by FAI scripts
if [ -n "${uplink_iface}" ] && [ -z "${UPLINK_IFACE}" ]; then

	do_d-e-r_preseed "debian-edu-router-config/net-ext-iface-uplink" "select" "${uplink_iface}"

	# Safely add "Uplink" or "Uplink, "
	do_d-e-r_preseed debian-edu-router-config/net-networks-dhcpclient-v4 multiselect \
		"$(add_multiselect_item debian-edu-router-config/net-networks-dhcpclient-v4 Uplink)"

	do_d-e-r_preseed debian-edu-router-config/net-networks-manual-v6 multiselect \
		"$(add_multiselect_item debian-edu-router-config/net-networks-manual-v6 Uplink)"

	# Filter out 'Uplink' for the rest.
	do_d-e-r_preseed debian-edu-router-config/net-networks-dhcpclient-v6 multiselect \
		"$(filter_multiselect_item debian-edu-router-config/net-networks-dhcpclient-v6 Uplink)"

	do_d-e-r_preseed debian-edu-router-config/net-networks-manual-v4 multiselect \
		"$(filter_multiselect_item debian-edu-router-config/net-networks-manual-v4 Uplink)"

	do_d-e-r_preseed debian-edu-router-config/net-networks-staticip-v4 multiselect \
		"$(filter_multiselect_item debian-edu-router-config/net-networks-staticip-v4 Uplink)"

	do_d-e-r_preseed debian-edu-router-config/net-networks-staticip-v6 multiselect \
		"$(filter_multiselect_item debian-edu-router-config/net-networks-staticip-v6 Uplink)"


# manually selected uplink NIC from install.GATEWAY.sh
elif [ -n "${UPLINK_IFACE}" ]; then
	do_d-e-r_preseed "debian-edu-router-config/net-ext-iface-uplink" "select" "${UPLINK_IFACE}"
	do_d-e-r_preseed "debian-edu-router-config/net-ext-address-v4-uplink" "string" "${UPLINK_FULL_ADDRESS_V4}"
	do_d-e-r_preseed "debian-edu-router-config/net-ext-gateway-v4-uplink" "string" "${UPLINK_GATEWAY_V4}"

	# Safely add "Uplink" or "Uplink, "
	do_d-e-r_preseed debian-edu-router-config/net-networks-staticip-v4 multiselect \
		"$(add_multiselect_item debian-edu-router-config/net-networks-staticip-v4 Uplink)"

	# Filter out 'Uplink' for the rest.
	do_d-e-r_preseed debian-edu-router-config/net-networks-dhcpclient-v4 multiselect \
		"$(filter_multiselect_item debian-edu-router-config/net-networks-dhcpclient-v4 Uplink)"

	do_d-e-r_preseed debian-edu-router-config/net-networks-dhcpclient-v6 multiselect \
		"$(filter_multiselect_item debian-edu-router-config/net-networks-dhcpclient-v6 Uplink)"

	do_d-e-r_preseed debian-edu-router-config/net-networks-manual-v6 multiselect \
		"$(filter_multiselect_item debian-edu-router-config/net-networks-manual-v6 Uplink)"

	do_d-e-r_preseed debian-edu-router-config/net-networks-manual-v4 multiselect \
		"$(filter_multiselect_item debian-edu-router-config/net-networks-manual-v4 Uplink)"

	do_d-e-r_preseed debian-edu-router-config/net-networks-staticip-v6 multiselect \
		"$(filter_multiselect_item debian-edu-router-config/net-networks-staticip-v6 Uplink)"
fi
