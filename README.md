# Debian Edu Router

This package contains the FAI config space for the Debian Edu Router
system. Details about Debian Edu Router and verbose instructions are
available in the Debian wiki at <URL:http://wiki.debian.org/DebianEdu/Router>.

There are two ways available to install the Debian Edu Router:

  - prepare a CD image and install from that image

or

  - install a minimal Debian and convert that installation

Choose the method appropriate for your situation.


## Creating a CD image with fai-cd

Create the CD image in the following way:

    apt install aptitude  # needed for FAI
    aptitude install debian-edu-router-fai
    mkdir /srv/fai/
    ln -s /usr/share/debian-edu-router/fai/config /srv/fai/

If you plan to adapt the config space to your needs then copy the FAI
config space instead of symlinking to it:

    cp -r /usr/share/debian-edu-router/fai/config /srv/fai/

Then, edit ``/etc/debian-edu/debian-edu-router-fai.conf`` and adjust to
your needs. Also the defaults should work fine, so this step can be
skipped for trying out Debian Edu Router.

Next, you can create the FAI nfsroot:

    debian-edu-router-fai_install

For USB flash drive or optical disc based deployments, a CD image can be
created using the fai-cd command:

    debian-edu-router-faicd

You can afterwards find the newly mastered CD image(s) in
``/srv/fai/iso-images/``.

You can then write the ``debian-edu-router.iso`` installer to
a USB flash drive and boot the installer from USB:

    dd if=debian-edu-router.img of=/dev/sd<X> status=progress

(Make sure to adjust ``/dev/sd<X>`` by the device name of your USB flash
drive).

Boot an empty x86 machine (or virtual machine for testing) from this USB
flash drive now and follow the instructions displayed on screen.


## Provisioning Debian Edu Router via http:// Protocol

Alternatively, the Debian Edu Router can also be deployed via http(s)://
protocol.

Install a fresh minimal Debian system on a public internet host.

You can then prepare a deployment webserver in the following way:

    apt install aptitude  # needed for FAI
    aptitude install debian-edu-router-fai
    aptitude -R install apache2
    mkdir /srv/fai/
    ln -s /usr/share/debian-edu-router/fai/config /srv/fai/

If you plan to adapt the config space to your needs then copy the FAI
config space instead of symlinking to it:

    cp -r /usr/share/debian-edu-router/fai/config /srv/fai/

Then, edit ``/etc/debian-edu/debian-edu-router-fai.conf`` and adjust to
your needs. Also the defaults should work fine, so this step can be
skipped for trying out Debian Edu Router.

Next, you can create the FAI nfsroot:

    debian-edu-router-fai_install

For provisioning Debian Edu Router via http(s):// protocol, you need the
FAI installer as squashfs image file:

    debian-edu-router-faiimage

You can afterwards find the newly created squashfs image(s) in
``/srv/fai/squashfs-images/``.

FIXME: The web download of the installer and the bootloader files still
needs to be documented.


## Converting a minimal Debian installation

Install a minimal Debian (only the core system with standard system
utilities) on the server.  Choose 'gateway' as hostname.  Prepare
appropriate partitions, examples are available in
/usr/share/debian-edu-router/fai/config/disk_config/.

Then convert the installation with the following commands:

    apt install aptitude  # needed for FAI
    aptitude install debian-edu-router-fai
    mkdir /srv/fai/
    ln -s /usr/share/debian-edu-router/fai/config /srv/fai/

If you plan to adapt the config space to your needs then copy the FAI
config space instead of symlinking to it:

    cp -r /usr/share/debian-edu-router/fai/config /srv/fai/

Adapt the config space to your needs if necessary and run FAI with the
variable CONVERT set to true:

    debian-edu-router-faisoftupdate

Reboot after conversion and follow the instructions displayed as login
message.


## Further Notes

If you prefer not to use the hostname 'gateway' but $HOSTNAME, use
the following commands to replace it in the fai config space:

    cd /srv/fai/config/
    for FILE in $(grep -rl gateway *) ; \
        do sed -i "s/gateway/$HOSTNAME/g" $FILE ; done

    mv files/etc/hosts/gateway files/etc/hosts/$HOSTNAME
    cd -
