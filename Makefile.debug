# IP of d-e-r debugging/test server
ip = d-e-r-machine.local
debugging_server_root_password = "12345678"

package_version = $(shell /usr/bin/dpkg-parsechangelog -S version)
CODENAME_DEBUG ?= bookworm
selected_plugins ?= d-e-r-p.m-r d-e-r-p.c-f
control_socket ?= /tmp/ssh_socket.d-e-r
destination_dir ?= /root/d-e-r-packages

# You can either edit the variables above or you can optionally store your
# personal settings in a 'env_file' file. This file is also listed in .gitignore.
# Just make sure you are using the following format for each variable:
#	export ip="<IP or hostname here>"
-include env_file

all:          build_wrapper upload_d-e-r upload_plugins install_d-e-r install_plugins
noplugins:    build_wrapper upload_d-e-r                install_d-e-r
onlyplugins:  build_wrapper              upload_plugins               install_plugins
upload:       build_wrapper upload_d-e-r upload_plugins

# Generate targets for uploading and installing the plugins packages
upload_plugins:  $(addprefix upload_,$(selected_plugins))
install_plugins: $(addprefix install_,$(selected_plugins))


# We use the SSH control socket feature to archieve a lower installation/upload time.
controlsocket:
	-rm -fv "$(control_socket)"
	-ssh -M -S "$(control_socket)" -o ControlPersist=5m root@$(ip) exit


# Experimental:
watch: controlsocket \
	# Step 1: Find files which are interesting
	# Step 2: Watch files for changes
	# Step 3: Upload file to the debug machine
	find . -name "debian-edu-router-config.*" -not -name "*.templates" -not -name "*.po" -not -name "*.pot" | \
	entr scp /_ -o "ControlPath=$(control_socket)" root@$(ip):/var/lib/dpkg/info/


build_wrapper: controlsocket clean build upload_d-e-r
build: clean \
	../debian-edu-router-fai_$(package_version)_all.deb \
	../debian-edu-router-deployserver_$(package_version)_all.deb \
	../debian-edu-router-config_$(package_version)_all.deb \
	../debian-edu-router-common_$(package_version)_all.deb \
	../debian-edu-router-plugin.mdns-reflector_$(package_version)_all.deb \
	../debian-edu-router-plugin.content-filter_$(package_version)_all.deb \
	../debian-edu-router-plugin.ldap-connector_$(package_version)_all.deb \
	../debian-edu-router-plugin.krb5-connector_$(package_version)_all.deb

../%.deb:
#	touch ../debian-edu-router-deployserver_$(package_version)_all.deb
#	touch ../debian-edu-router-fai_$(package_version)_all.deb
#	touch ../debian-edu-router-config_$(package_version)_all.deb
#	touch ../debian-edu-router-common_$(package_version)_all.deb
#	touch ../debian-edu-router-plugin.mdns-reflector_$(package_version)_all.deb
#	touch ../debian-edu-router-plugin.content-filter_$(package_version)_all.deb
#	touch ../debian-edu-router-plugin.ldap-connector_$(package_version)_all.deb
#	touch ../debian-edu-router-plugin.krb5-connector_$(package_version)_all.deb
	debuild --no-lintian -uc -us -d -S
	cd .. && sbuild --no-run-lintian -sAd $(CODENAME_DEBUG) *_$(package_version).dsc


clean:
	ssh -S "$(control_socket)" root@$(ip) "rm -fv $(destination_dir)/*.deb"
	-cd .. && rm -v *.deb *.changes *.build *.buildinfo *.dsc *.tar.xz *.iso *.upload

ensure_destination_dir:
	ssh -S "$(control_socket)" root@$(ip) "mkdir '$(destination_dir)' -p"

upload_d-e-r: ../debian-edu-router-config_$(package_version)_all.deb ../debian-edu-router-common_$(package_version)_all.deb ensure_destination_dir
	-cd .. && ssh -S "$(control_socket)" root@$(ip) "rm debian-edu-router-config_$(package_version)_all.deb"
	-cd .. && ssh -S "$(control_socket)" root@$(ip) "rm debian-edu-router-common_$(package_version)_all.deb"
	cd .. && scp -o "ControlPath=$(control_socket)" debian-edu-router-config_$(package_version)_all.deb root@$(ip):$(destination_dir)
	cd .. && scp -o "ControlPath=$(control_socket)" debian-edu-router-common_$(package_version)_all.deb root@$(ip):$(destination_dir)

upload_d-e-r-p.m-r: ../debian-edu-router-plugin.mdns-reflector_$(package_version)_all.deb ensure_destination_dir
	cd .. && scp -o "ControlPath=$(control_socket)" debian-edu-router-plugin.mdns-reflector_$(package_version)_all.deb root@$(ip):$(destination_dir)

upload_d-e-r-p.c-f: ../debian-edu-router-plugin.content-filter_$(package_version)_all.deb ensure_destination_dir
	cd .. && scp -o "ControlPath=$(control_socket)" debian-edu-router-plugin.content-filter_$(package_version)_all.deb root@$(ip):$(destination_dir)

upload_d-e-r-p.l-c: ../debian-edu-router-plugin.ldap-connector_$(package_version)_all.deb ensure_destination_dir
	cd .. && scp -o "ControlPath=$(control_socket)" debian-edu-router-plugin.ldap-connector_$(package_version)_all.deb root@$(ip):$(destination_dir)

upload_d-e-r-p.k-c: ../debian-edu-router-plugin.krb5-connector_$(package_version)_all.deb ensure_destination_dir
	cd .. && scp -o "ControlPath=$(control_socket)" debian-edu-router-plugin.krb5-connector_$(package_version)_all.deb root@$(ip):$(destination_dir)


uninstall_d-e-r:
	-ssh -S "$(control_socket)" root@$(ip) "apt-get remove -y debian-edu-router-config debian-edu-router-common"

uninstall_d-e-r-p.m-r:
	-ssh -S "$(control_socket)" root@$(ip) "apt-get remove -y debian-edu-router-plugin.mdns-reflector"

uninstall_d-e-r-p.c-f:
	-ssh -S "$(control_socket)" root@$(ip) "apt-get remove -y debian-edu-router-plugin.content-filter"

uninstall_d-e-r-p.l-c:
	-ssh -S "$(control_socket)" root@$(ip) "apt-get remove -y debian-edu-router-plugin.ldap-connector"

uninstall_d-e-r-p.k-c:
	-ssh -S "$(control_socket)" root@$(ip) "apt-get remove -y debian-edu-router-plugin.krb5-connector"


install_d-e-r: uninstall_d-e-r
	ssh -S "$(control_socket)" root@$(ip) "export D_E_R_DEBUG=true; export DEBIAN_FRONTEND=noninteractive; apt-get install -y $(destination_dir)/debian-edu-router-config_$(package_version)_all.deb $(destination_dir)/debian-edu-router-common_$(package_version)_all.deb"

install_d-e-r-p.m-r: install_d-e-r
	ssh -S "$(control_socket)" root@$(ip) "export D_E_R_DEBUG=true; export DEBIAN_FRONTEND=noninteractive; apt-get reinstall -y $(destination_dir)/debian-edu-router-plugin.mdns-reflector_$(package_version)_all.deb"

install_d-e-r-p.c-f: install_d-e-r
	ssh -S "$(control_socket)" root@$(ip) "export D_E_R_DEBUG=true; export DEBIAN_FRONTEND=noninteractive; apt-get reinstall -y $(destination_dir)/debian-edu-router-plugin.content-filter_$(package_version)_all.deb"

install_d-e-r-p.l-c: install_d-e-r
	ssh -S "$(control_socket)" root@$(ip) "export D_E_R_DEBUG=true; export DEBIAN_FRONTEND=noninteractive; apt-get reinstall -y $(destination_dir)/debian-edu-router-plugin.ldap-connector_$(package_version)_all.deb"

install_d-e-r-p.k-c: install_d-e-r
	ssh -S "$(control_socket)" root@$(ip) "export D_E_R_DEBUG=true; export DEBIAN_FRONTEND=noninteractive; apt-get reinstall -y $(destination_dir)/debian-edu-router-plugin.krb5-connector_$(package_version)_all.deb"

prepare_debugging_server:
	# Manual Steps:
	#   1. Disable uif on debugging server (temporary)
	#   2. Enable root login (with password!) in /etc/ssh/sshd_config (temporary)
	echo $(debugging_server_root_password) | ssh-copy-id root@$(ip)
	ssh -S "$(control_socket)" root@$(ip) "apt update; apt install devscripts dnsutils git git-extras build-essential nano mc vim sudo ipcalc ipv6calc iproute2 gpm avahi-utils rsync etckeeper nmap -y"
	# Enable debugging on this server.
	ssh -S "$(control_socket)" root@$(ip) "touch /etc/debian-edu-router/debug"
