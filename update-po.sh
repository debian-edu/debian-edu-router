#!/bin/bash

# Copyright (C) 2017 by Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# This package is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3 of the License.
#
# This package is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>

cat po/DOMAINS | while read GETTEXT_DOMAIN; do
	cp po/${GETTEXT_DOMAIN}.pot po/${GETTEXT_DOMAIN}.pot~
	cp po/POTFILES.${GETTEXT_DOMAIN}.in po/POTFILES.in
	cd po/
	cat LINGUAS | while read lingua; do
		if [[ ! -e ${lingua}/LC_MESSAGES/${GETTEXT_DOMAIN}.po || ! -s ${lingua}/LC_MESSAGES/${GETTEXT_DOMAIN}.po ]]; then
			echo "Creating ${lingua}/LC_MESSAGES/${GETTEXT_DOMAIN}.po file..."
			mkdir -p "${lingua}/LC_MESSAGES/"
			msginit --input=${GETTEXT_DOMAIN}.pot --width 79 --locale=${lingua} --no-translator --output-file=${lingua}/LC_MESSAGES/${GETTEXT_DOMAIN}.po
		else
			echo "Updating ${lingua}/LC_MESSAGES/${GETTEXT_DOMAIN}.po file..."
			intltool-update --gettext-package ${GETTEXT_DOMAIN} ${lingua}/LC_MESSAGES/${GETTEXT_DOMAIN}
		fi
	done
	cd - 1>/dev/null

	mv po/${GETTEXT_DOMAIN}.pot~ po/${GETTEXT_DOMAIN}.pot
done

rm -f po/POTFILES.in
