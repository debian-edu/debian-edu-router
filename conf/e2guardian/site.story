### THIS FILE IS MANAGED BY DEBIAN-EDU-ROUTER PLUGIN: CONTENT FILTER

### DON'T MODIFY THIS FILE - IT GETS OVERWRITTEN ON PACKAGE UPGRADES AND WHILE RECONFIGURATING
### This file is part of the debian-edu-router-plugin.content-filter DEB package.

# This is the site level storybook

# override library functions and add your site level functions here

#Examples:-

# General:-

# If you do not use local files then uncomment the following lines:-
#function(localcheckrequest)
#function(localsslrequestcheck)
#function(localgreycheck)
#function(localbannedcheck)
#function(localexceptioncheck)
#function(localsslcheckrequest)

# To disable checks on embedded urls then uncomment:-
#function(embeddedcheck)

# If you have av scanning enabled then comment out next 2 lines:-
function(checknoscanlists)
function(checknoscantypes)

# If you only want exception extensions/mime filetypes to be allowed
# then uncomment the following 4 lines
#function(checkfiletype)
#if(mimein, exceptionmime) return false
#if(extensionin, exceptionextension) return false
#if(true) return setblock

# To override the nolog lists temporarily
# then uncomment the following two lines
#function(checklogging)
#if(true) return true


# For ICAP mode:-

# If you are using squid bump for https interception uncomment
# next 2 lines
function(icapsquidbump)
if(true) return true
