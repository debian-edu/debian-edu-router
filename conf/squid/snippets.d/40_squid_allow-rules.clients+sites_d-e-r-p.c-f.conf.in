# Debian Edu Router Plugin: Content Filter

# Allow every request from local browsers, but still do content introspection.
# Just disable proxy in the browser to avoid deep content introspection.
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ allow localhost

# [Trusted access mode] No filtering at all, not even deep content introspection, nor SSL bumping
# This can be achieved for the entire network by dpkg-reconfiguring debian-edu-router-config
# and enabling direct internet access.
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ allow ProxyTrustedClient
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ allow ProxyTrustedClientIP

# [Deny access mode] Deny everything
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ deny ProxyDenyClient
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ deny ProxyDenyClientIP

# DENY SITES: Deny everything (Except for authenticated trusted users)
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ deny ProxyDenySite
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ deny ProxyDenySiteRegex
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ deny ProxyDenySiteIP
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ deny ProxyDenySiteIP_Generated transparent_mode
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ deny ProxyDenyURL

# [All access modes] except [Deny mode]: Allow always allowed websites.
# For example school websites.
# Theses websites should be safe for minors + shouldn't contain test answers ;)
# Do not include file sharing hosts here (e.g. Nextcloud, Dropbox, Drive), that goes into ProxyWhitelist*!
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ allow ProxyAllowSite
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ allow ProxyAllowSiteRegex
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ allow ProxyAllowSiteIP
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ allow ProxyAllowSiteIP_Generated transparent_mode
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ allow ProxyAllowURL

# ALLOW SITES: Allow everything (but SSL bump and cache content)
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ allow ProxyAllowClient
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ allow ProxyAllowClientIP

# [Whitelist access mode] Allow only whitelisted websites.
# For example math tools, search engine, large language models, etc...
# Theses websites should be safe for minors though.
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ allow ProxyWhitelistClient   ProxyWhitelistSite
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ allow ProxyWhitelistClientIP ProxyWhitelistSite
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ allow ProxyWhitelistClient   ProxyWhitelistSiteRegex
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ allow ProxyWhitelistClientIP ProxyWhitelistSiteRegex
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ allow ProxyWhitelistClient   ProxyWhitelistSiteIP
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ allow ProxyWhitelistClientIP ProxyWhitelistSiteIP
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ allow ProxyWhitelistClient   ProxyWhitelistSiteIP_Generated transparent_mode
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ allow ProxyWhitelistClientIP ProxyWhitelistSiteIP_Generated transparent_mode
# DISABLED. Please see /usr/share/doc/debian-edu-router-plugin.content-filter/squid_ACLs_explained.md at "Overview of all site lists"
#@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ allow ProxyWhitelistClient   ProxyWhitelistURL
#@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ allow ProxyWhitelistClientIP ProxyWhitelistURL

# [Whitelist access mode] Deny every other website (including ProxyBlacklist*).
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ deny ProxyWhitelistClient
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ deny ProxyWhitelistClientIP

# [Blacklist access mode] Deny all blacklisted websites.
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ deny ProxyBlacklistClient   ProxyBlacklistSite
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ deny ProxyBlacklistClientIP ProxyBlacklistSite
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ deny ProxyBlacklistClient   ProxyBlacklistSiteRegex
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ deny ProxyBlacklistClientIP ProxyBlacklistSiteRegex
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ deny ProxyBlacklistClient   ProxyBlacklistSiteIP
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ deny ProxyBlacklistClientIP ProxyBlacklistSiteIP
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ deny ProxyBlacklistClient   ProxyBlacklistSiteIP_Generated transparent_mode
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ deny ProxyBlacklistClientIP ProxyBlacklistSiteIP_Generated transparent_mode
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ deny ProxyBlacklistClient   ProxyBlacklistURL
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ deny ProxyBlacklistClientIP ProxyBlacklistURL

# [Blacklist access mode] Allow every other website (which are not in ProxyBlacklist{URL, Site}*).
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ allow ProxyBlacklistClient
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ allow ProxyBlacklistClientIP

# There are devices which shall not be asked to authenticate.
@HTTP_OR_CACHEPEER_ACCESS_UPSTREAM@ deny ProxyNoauthClient
