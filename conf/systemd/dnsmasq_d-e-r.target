# Copyright (C) 2023 Pädagogisches Landesinstitut Rheinland-Pfalz
# Copyright (C) 2023 Daniel Teichmann <daniel.teichmann@das-netzwerkteam.de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

#
# This systemd target is used to hold all Dnsmasq instances managed by
# the Debian-Edu-Router together and to orchestrate all dnsmasq-services at
# once.

[Unit]
Description=dnsmasq - A DHCP/DNS server (managed by Debian-Edu-Router)
After=network.target network-online.target nss-lookup.target dnsmasq.service
Conflicts=dnsmasq.service

# This collection of dnsmasq services should be started at boot time.
[Install]
WantedBy=multi-user.target
