# Copyright (C) 2024 Pädagogisches Landesinstitut Rheinland-Pfalz
# Copyright (C) 2024 Daniel Teichmann <daniel.teichmann@das-netzwerkteam.de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
Alias /debian-edu-router/ssl/certs /etc/debian-edu-router/ssl/certs

<Directory "/etc/debian-edu-router/ssl/certs">
	Options +FollowSymLinks +MultiViews +Indexes
	require all granted
</Directory>

Alias /index.html /etc/debian-edu-router/www/index.html
Alias /wpad.dat   /etc/debian-edu-router/www/wpad.dat

<Directory "/etc/debian-edu-router/www/">
	Options +FollowSymLinks +MultiViews +Indexes
	require all granted
</Directory>

# Make sure that custom HTTP/HTTPS ports are working.
# If the next line is empty, the default port 80 will be used. Please see /etc/apache2/ports.conf.
@APACHE2_LISTEN_HTTP_PORT@
<IfModule ssl_module>
	# If the next line is empty, the default port 443 will be used. Please see /etc/apache2/ports.conf.
	@APACHE2_LISTEN_HTTPS_PORT@
</IfModule>
<IfModule mod_gnutls.c>
	# If the next line is empty, the default port 443 will be used. Please see /etc/apache2/ports.conf.
	@APACHE2_LISTEN_HTTPS_PORT@
</IfModule>

# Do not overshare possible sensitive information about this web server.
ServerSignature Off
ServerTokens Prod
