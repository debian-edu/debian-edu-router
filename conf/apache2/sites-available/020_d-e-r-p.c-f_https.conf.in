# Copyright (C) 2024 Pädagogisches Landesinstitut Rheinland-Pfalz
# Copyright (C) 2024 Daniel Teichmann <daniel.teichmann@das-netzwerkteam.de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

<VirtualHost *:@APACHE2_HTTPS_PORT@>
    ServerAdmin webmaster@localhost

    DocumentRoot /etc/debian-edu-router/www/

    ErrorLog /var/log/debian-edu-router/apache2/error_d-e-r_default_@APACHE2_HTTPS_PORT@.log
    CustomLog /var/log/debian-edu-router/apache2/access_d-e-r_default_@APACHE2_HTTPS_PORT@.log combined

    #   SSL Engine Switch:
    #   Enable/Disable SSL for this virtual host.
    SSLEngine on

    # We are using D-E-R-P.C-F's self-signed SSL certificate.
    @APACHE2_SELFSIGNED_CERT@SSLCertificateFile      /etc/debian-edu-router/ssl/certs/d-e-r_sslmitm-ca.crt
    @APACHE2_SELFSIGNED_CERT@SSLCertificateKeyFile   /etc/debian-edu-router/ssl/private/d-e-r_sslmitm-ca.key

    # But if that is not available, use self-signed (snakeoil) certificate
    # created by the ssl-cert package.
    # See /usr/share/doc/apache2/README.Debian.gz
    @APACHE2_SNAKEOIL_CERT@SSLCertificateFile      /etc/ssl/certs/ssl-cert-snakeoil.pem
    @APACHE2_SNAKEOIL_CERT@SSLCertificateKeyFile   /etc/ssl/private/ssl-cert-snakeoil.key

    #   SSL Engine Options:
    #   Set various options for the SSL engine.
    #   o FakeBasicAuth:
    #    Translate the client X.509 into a Basic Authorisation.  This means that
    #    the standard Auth/DBMAuth methods can be used for access control.  The
    #    user name is the `one line' version of the client's X.509 certificate.
    #    Note that no password is obtained from the user. Every entry in the user
    #    file needs this password: `xxj31ZMTZzkVA'.
    #   o ExportCertData:
    #    This exports two additional environment variables: SSL_CLIENT_CERT and
    #    SSL_SERVER_CERT. These contain the PEM-encoded certificates of the
    #    server (always existing) and the client (only existing when client
    #    authentication is used). This can be used to import the certificates
    #    into CGI scripts.
    #   o StdEnvVars:
    #    This exports the standard SSL/TLS related `SSL_*' environment variables.
    #    Per default this exportation is switched off for performance reasons,
    #    because the extraction step is an expensive operation and is usually
    #    useless for serving static content. So one usually enables the
    #    exportation for CGI and SSI requests only.
    #   o OptRenegotiate:
    #    This enables optimized SSL connection renegotiation handling when SSL
    #    directives are used in per-directory context.
    #SSLOptions +FakeBasicAuth +ExportCertData +StrictRequire
    <FilesMatch "\.(?:cgi|shtml|phtml|php)$">
        SSLOptions +StdEnvVars
    </FilesMatch>
    <Directory /usr/lib/cgi-bin>
        SSLOptions +StdEnvVars
    </Directory>
</VirtualHost>