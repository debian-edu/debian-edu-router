#!/bin/sh

# Debian Edu Router Plugin: LDAP/AD Connector.
# Documentation can be found under: /usr/share/doc/debian-edu-router*/
#
# Copyright (C) 2024 Pädagogisches Landesinstitut Rheinland-Pfalz
# Copyright (C) 2024 Daniel Teichmann <daniel.teichmann@das-netzwerkteam.de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

##########################################################################################
# This script should be used in a Debian Edu network (using Tjener)                      #
# If you are operating outside of a Debian Edu network,                                  #
# you can add a new script into this directory, which gets the                           #
# CA certificate in some other way.                                                      #
#                                                                                        #
# Verify that your script will be selected with:                                         #
# 'find . -maxdepth 1 -name "*.sh" -type f -executable  | sort | tail -n1'.              #
# Only one script will be selected to get the CA certificate.                            #
##########################################################################################

# Just output certificate to stdout.
# stderr will be ignored, if valid certifcate was provided.
curl www.intern/Debian-Edu_rootCA.crt -qLk -o -
