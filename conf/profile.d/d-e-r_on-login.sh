#!/bin/bash

# Copyright (C) 2010-2023 Pädagogisches Landesinstitut Rheinland-Pfalz
# Copyright (C) 2022-2023 Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

# Assume that we don't want the login menu if SHLVL is unset.
# Observed when invoking "su - <user>".
if ! [ "$SHLVL" ]; then
	SHLVL=2
fi

if [ "$PS1" ]; then
	if [ "`id -u`" -eq 0 -a "$SHLVL" -le 1 -a -n "$(tty)" ] && \
	   [ -x /usr/sbin/debian-edu-router-loginmenu ]; then
		exec /usr/sbin/debian-edu-router-loginmenu
	fi
fi
