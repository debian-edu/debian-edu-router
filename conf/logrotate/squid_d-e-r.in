# Copyright (C) 2024 Pädagogisches Landesinstitut Rheinland-Pfalz
# Copyright (C) 2024 Daniel Teichmann <daniel.teichmann@das-netzwerkteam.de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

#
# Logrotate fragment for D-E-R managed Squid instance of network '@NETWORK_NAME@'.
#

/var/log/debian-edu-router/squid/*@NETWORK_NAME@*.log {
    daily
    compress
    delaycompress
    rotate 2
    missingok
    notifempty
    create 640 proxy proxy
    sharedscripts
    prerotate
        test ! -x /usr/sbin/sarg-reports || /usr/sbin/sarg-reports daily
    endscript
    postrotate
        if pgrep -f ^/usr/sbin/squid -a | grep @NETWORK_NAME@ > /dev/null; then
            /usr/sbin/squid -k rotate -f /etc/squid/conf.d/debian-edu-router-plugin.content-filter/00_d-e-r-p.c-f_@NETWORK_NAME@.conf > /dev/null 2>&1
            invoke-rc.d squid_d-e-r@@NETWORK_NAME@ reload 2>&1 | logger -t squid_d-e-r@@NETWORK_NAME@.logrotate
        fi
    endscript
}
