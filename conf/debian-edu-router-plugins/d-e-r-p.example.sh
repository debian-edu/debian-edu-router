# to be sourced

# Copyright (C) 2023-2024 Pädagogisches Landesinstitut Rheinland-Pfalz
# Copyright (C) 2023-2024 Daniel Teichmann <daniel.teichmann@das-netzwerkteam.de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

#
# This file defines a plugin for the »Debian Edu Router« login menu.
# This file should not be executed manually, it should be automatically
# sourced by debian-edu-router-loginmenu.
#

# source gettext tools
. gettext.sh

# Needed for gettext translations
export TEXTDOMAIN="debian-edu-router-plugin.example-plugin"

# Set product name defaults "Debian Edu Router Plugin"
# Normally these settings should be defined in /etc/debian-edu/router.conf.d/
PRODUCTNAME="${PRODUCTNAME:-"$(eval_gettext "Debian Edu Router")"}"
PRODUCTNAME_PLUGIN_SUFFIX="${PRODUCTNAME_PLUGIN_SUFFIX:-"$(eval_gettext "Plugin")"}"

# Debian Edu Router Plugin: Content filter
PLUGIN_NAME="`printf "$(eval_gettext "%s %s: Example plugin")" "$PRODUCTNAME" "$PRODUCTNAME_PLUGIN_SUFFIX"`"
PLUGIN_DESCRIPTION="$(eval_gettext "This is just an example plugin")"
PLUGIN_IS_ENABLED_FILE="/path/to/example/enabled"
PLUGIN_PACKAGE_NAME="debian-edu-router-plugin.example-plugin"
PLUGIN_PACKAGE_VERSION="$(dpkg -s $PLUGIN_PACKAGE_NAME | grep -i version | grep -vi Config-Version | awk '{ print $2 }')"

#
# PLUGIN_CONF_SUBMENU_ITEMS -> Declares $ITEM for below (used to keep things organized) (can be left empty)
#
# For each entry in PLUGIN_CONF_SUBMENU_ITEMS:
#   PLUGIN_CONF_SUBMENU_ITEM_COMMAND_$ITEM - Command to be executed by bash when activating this menu item.
#                                            Please be careful with these commands, since they will be executed by the root user!
#   PLUGIN_CONF_SUBMENU_ITEM_TEXT_$ITEM    - For example: "Debian Edu Router Plugin: Example plugin Configuration"
#   PLUGIN_CONF_SUBMENU_ITEM_KEY_$ITEM     - The key which should be pressed to activate a submenu item. (Please be aware of different keyboard layouts)

# Example configuration:
PLUGIN_CONF_SUBMENU_ITEMS=("ALL" "ONOFF" "PARTLY" "TASK1" "TEST")

PLUGIN_CONF_SUBMENU_ITEM_COMMAND_ALL="dpkg-reconfigure $PLUGIN_PACKAGE_NAME"
PLUGIN_CONF_SUBMENU_ITEM_TEXT_ALL="`printf "$(eval_gettext "Configure entirely")"`"
PLUGIN_CONF_SUBMENU_ITEM_KEY_ALL="a"

PLUGIN_CONF_SUBMENU_ITEM_COMMAND_ONOFF="CONFIGURE_ONLY=ONOFF dpkg-reconfigure $PLUGIN_PACKAGE_NAME"
PLUGIN_CONF_SUBMENU_ITEM_TEXT_ONOFF="`printf "$(eval_gettext "Toggle example's functionality on/off")"`"
PLUGIN_CONF_SUBMENU_ITEM_KEY_ONOFF="t"

PLUGIN_CONF_SUBMENU_ITEM_COMMAND_PARTLY="CONFIGURE_ONLY=PARTLY dpkg-reconfigure $PLUGIN_PACKAGE_NAME"
PLUGIN_CONF_SUBMENU_ITEM_TEXT_PARTLY="`printf "$(eval_gettext "Configure only partly")"`"
PLUGIN_CONF_SUBMENU_ITEM_KEY_PARTLY="p"

PLUGIN_CONF_SUBMENU_ITEM_COMMAND_TASK1="SKIP_DEBCONF_QUESTIONS_CONFIG=1 DO_TASK1=true dpkg-reconfigure $PLUGIN_PACKAGE_NAME"
if [[ -s "/var/lib/debian-edu-router/d-e-r-p.example/task1.enabled" ]]; then
    PLUGIN_CONF_SUBMENU_ITEM_TEXT_TASK1="`printf "$(eval_gettext "[X] Switch task off: Example Task 1.")"`"
else
    PLUGIN_CONF_SUBMENU_ITEM_TEXT_TASK1="`printf "$(eval_gettext "[ ] Switch task on: Example Task 1.")"`"
fi
PLUGIN_CONF_SUBMENU_ITEM_KEY_TASK1="i"

PLUGIN_CONF_SUBMENU_ITEM_COMMAND_TEST="echo Debian Edu Router is amazing! :P"
PLUGIN_CONF_SUBMENU_ITEM_TEXT_TEST="`printf "$(eval_gettext "Do things and stuff")"`"
PLUGIN_CONF_SUBMENU_ITEM_KEY_TEST="t"
