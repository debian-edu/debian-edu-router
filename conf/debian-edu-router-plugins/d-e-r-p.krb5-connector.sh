# to be sourced

# Copyright (C) 2024 Pädagogisches Landesinstitut Rheinland-Pfalz
# Copyright (C) 2024 Daniel Teichmann <daniel.teichmann@das-netzwerkteam.de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

#
# This file defines a plugin for the »Debian Edu Router« login menu.
# This file should not be executed manually, it should be automatically
# sourced by debian-edu-router-loginmenu.
#

# source gettext tools
. gettext.sh

# Needed for gettext translations
export TEXTDOMAIN="debian-edu-router-plugin.krb5-connector"

# Set product name defaults "Debian Edu Router Plugin"
# Normally these settings should be defined in /etc/debian-edu/router.conf.d/
PRODUCTNAME="${PRODUCTNAME:-"$(eval_gettext "Debian Edu Router")"}"
PRODUCTNAME_PLUGIN_SUFFIX="${PRODUCTNAME_PLUGIN_SUFFIX:-"$(eval_gettext "Plugin")"}"

# Debian Edu Router Plugin: Krb5 Connector
PLUGIN_NAME="`printf "$(eval_gettext "%s %s: Krb5 Connector")" "$PRODUCTNAME" "$PRODUCTNAME_PLUGIN_SUFFIX"`"
PLUGIN_DESCRIPTION="$(eval_gettext "Provides the possibility to authenticate using Kerberos tickets.")"
PLUGIN_IS_ENABLED_FILE="/var/lib/debian-edu-router/d-e-r-p.k-c/enabled"
PLUGIN_PACKAGE_NAME="debian-edu-router-plugin.krb5-connector"
PLUGIN_PACKAGE_VERSION="$(dpkg -s $PLUGIN_PACKAGE_NAME | grep -i version | grep -vi Config-Version | awk '{ print $2 }')"

#
# PLUGIN_CONF_SUBMENU_ITEMS -> Declares $ITEM for below (used to keep things organized) (can be left empty)
#
# For each entry in PLUGIN_CONF_SUBMENU_ITEMS:
#   PLUGIN_CONF_SUBMENU_ITEM_COMMAND_$ITEM - Command to be executed by bash when activating this menu item.
#                                            Please be careful with these commands, since they will be executed by the root user!
#   PLUGIN_CONF_SUBMENU_ITEM_TEXT_$ITEM    - For example: "Debian Edu Router Plugin: Example plugin Configuration"
#   PLUGIN_CONF_SUBMENU_ITEM_KEY_$ITEM     - The key which should be pressed to activate a submenu item. (Please be aware of different keyboard layouts)

# Example configuration:
PLUGIN_CONF_SUBMENU_ITEMS=("ALL" "ONOFF" "INSTALL_SERVICE_TICKET")

PLUGIN_CONF_SUBMENU_ITEM_COMMAND_ALL="dpkg-reconfigure $PLUGIN_PACKAGE_NAME"
PLUGIN_CONF_SUBMENU_ITEM_TEXT_ALL="`printf "$(eval_gettext "Configure entirely")"`"
PLUGIN_CONF_SUBMENU_ITEM_KEY_ALL="a"

PLUGIN_CONF_SUBMENU_ITEM_COMMAND_ONOFF="CONFIGURE_ONLY=ONOFF dpkg-reconfigure $PLUGIN_PACKAGE_NAME"
PLUGIN_CONF_SUBMENU_ITEM_TEXT_ONOFF="`printf "$(eval_gettext "Toggle Krb5 connector's functionality on/off")"`"
PLUGIN_CONF_SUBMENU_ITEM_KEY_ONOFF="t"

PLUGIN_CONF_SUBMENU_ITEM_COMMAND_INSTALL_SERVICE_TICKET="SKIP_DEBCONF_QUESTIONS_CONFIG=1 FORCE_INSTALL_SERVICE_TICKET=true dpkg-reconfigure $PLUGIN_PACKAGE_NAME"
PLUGIN_CONF_SUBMENU_ITEM_TEXT_INSTALL_SERVICE_TICKET="`printf "$(eval_gettext "Install HTTP proxy service ticket on Kerberos server.")"`"
PLUGIN_CONF_SUBMENU_ITEM_KEY_INSTALL_SERVICE_TICKET="b"
