# to be sourced

# Copyright (C) 2023-2024 Pädagogisches Landesinstitut Rheinland-Pfalz
# Copyright (C) 2023-2024 Daniel Teichmann <daniel.teichmann@das-netzwerkteam.de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

#
# This file defines a plugin for the »Debian Edu Router« login menu.
# This file should not be executed manually, it should be automatically
# sourced by debian-edu-router-loginmenu.
#

# source gettext tools
. gettext.sh

# Needed for gettext translations
export TEXTDOMAIN="debian-edu-router-plugin.mdns-reflector"

# Set product name defaults "Debian Edu Router Plugin"
PRODUCTNAME="${PRODUCTNAME:-"$(eval_gettext "Debian Edu Router")"}"
PRODUCTNAME_PLUGIN_SUFFIX="${PRODUCTNAME_PLUGIN_SUFFIX:-"$(eval_gettext "Plugin")"}"

# Debian Edu Router Plugin: mDNS reflector
PLUGIN_NAME="`printf "$(eval_gettext "%s %s: mDNS reflector")" "$PRODUCTNAME" "$PRODUCTNAME_PLUGIN_SUFFIX"`"
PLUGIN_DESCRIPTION="$(eval_gettext "This package adds an mDNS reflector service to Debian Edu Router.")"
PLUGIN_IS_ENABLED_FILE="/var/lib/debian-edu-router/d-e-r-p.m-r/enabled"
PLUGIN_PACKAGE_NAME="debian-edu-router-plugin.mdns-reflector"
PLUGIN_PACKAGE_VERSION="$(dpkg -s $PLUGIN_PACKAGE_NAME | grep -i version | grep -vi Config-Version | awk '{ print $2 }')"

# Define menu items for configuration submenu
# Please see example plugin configuration for explanation.
PLUGIN_CONF_SUBMENU_ITEMS=( "ALL" "ONOFF" )

PLUGIN_CONF_SUBMENU_ITEM_COMMAND_ALL="dpkg-reconfigure $PLUGIN_PACKAGE_NAME"
PLUGIN_CONF_SUBMENU_ITEM_TEXT_ALL="`printf "$(eval_gettext "Configure entirely")"`"
PLUGIN_CONF_SUBMENU_ITEM_KEY_ALL="a"

PLUGIN_CONF_SUBMENU_ITEM_COMMAND_ONOFF="CONFIGURE_ONLY=ONOFF dpkg-reconfigure $PLUGIN_PACKAGE_NAME"
PLUGIN_CONF_SUBMENU_ITEM_TEXT_ONOFF="`printf "$(eval_gettext "Toggle mDNS reflector's functionality on/off")"`"
PLUGIN_CONF_SUBMENU_ITEM_KEY_ONOFF="t"