# to be sourced

# Copyright (C) 2023-2024 Pädagogisches Landesinstitut Rheinland-Pfalz
# Copyright (C) 2023-2024 Daniel Teichmann <daniel.teichmann@das-netzwerkteam.de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

#
# This file defines a plugin for the »Debian Edu Router« login menu.
# This file should not be executed manually, it should be automatically
# sourced by debian-edu-router-loginmenu.
#

# source gettext tools
. gettext.sh

# Needed for gettext translations
export TEXTDOMAIN="debian-edu-router-plugin.content-filter"

# Set product name defaults "Debian Edu Router Plugin"
PRODUCTNAME="${PRODUCTNAME:-"$(eval_gettext "Debian Edu Router")"}"
PRODUCTNAME_PLUGIN_SUFFIX="${PRODUCTNAME_PLUGIN_SUFFIX:-"$(eval_gettext "Plugin")"}"

# Debian Edu Router Plugin: Content filter
PLUGIN_NAME="`printf "$(eval_gettext "%s %s: Content filter")" "$PRODUCTNAME" "$PRODUCTNAME_PLUGIN_SUFFIX"`"
PLUGIN_DESCRIPTION="$(eval_gettext "This plugin adds an squid/e2guardian based content filtering system for the internal networks.")"
PLUGIN_IS_ENABLED_FILE="/var/lib/debian-edu-router/d-e-r-p.c-f/enabled"
PLUGIN_PACKAGE_NAME="debian-edu-router-plugin.content-filter"
PLUGIN_PACKAGE_VERSION="$(dpkg -s $PLUGIN_PACKAGE_NAME | grep -i version | grep -vi Config-Version | awk '{ print $2 }')"

#
# PLUGIN_CONF_SUBMENU_ITEMS -> Declares $ITEM for below (used to keep things organized) (can be left empty)
#
# For each entry in PLUGIN_CONF_SUBMENU_ITEMS:
#   PLUGIN_CONF_SUBMENU_ITEM_COMMAND_$ITEM - Command to be executed by bash when activating this menu item.
#                                            Please be careful with these commands, since they will be executed by the root user!
#   PLUGIN_CONF_SUBMENU_ITEM_TEXT_$ITEM    - For example: "Debian Edu Router Plugin: Example plugin Configuration"
#   PLUGIN_CONF_SUBMENU_ITEM_KEY_$ITEM     - The key which should be pressed to activate a submenu item. (Please be aware of different keyboard layouts)

# Example configuration:
PLUGIN_CONF_SUBMENU_ITEMS=("ALL" "ONOFF" "FORCE_REWRITE_SSL_MITM_CA" "FORCE_SWITCH_R_BLISTS" "FORCE_SWITCH_R_GENIPS" "MANUALLY_REFRESH_BLISTS" "MANUALLY_REFRESH_GENIPS" "EDIT_SELECTED_CATEGORIES")

PLUGIN_CONF_SUBMENU_ITEM_COMMAND_ALL="dpkg-reconfigure $PLUGIN_PACKAGE_NAME"
PLUGIN_CONF_SUBMENU_ITEM_TEXT_ALL="`printf "$(eval_gettext "Configure entirely")"`"
PLUGIN_CONF_SUBMENU_ITEM_KEY_ALL="a"

PLUGIN_CONF_SUBMENU_ITEM_COMMAND_ONOFF="CONFIGURE_ONLY=ONOFF dpkg-reconfigure $PLUGIN_PACKAGE_NAME"
PLUGIN_CONF_SUBMENU_ITEM_TEXT_ONOFF="`printf "$(eval_gettext "Toggle content filter's functionality on/off")"`"
PLUGIN_CONF_SUBMENU_ITEM_KEY_ONOFF="t"

PLUGIN_CONF_SUBMENU_ITEM_COMMAND_FORCE_REWRITE_SSL_MITM_CA="CONFIGURE_ONLY=FORCE_REWRITE_SSL_MITM_CA dpkg-reconfigure $PLUGIN_PACKAGE_NAME"
PLUGIN_CONF_SUBMENU_ITEM_TEXT_FORCE_REWRITE_SSL_MITM_CA="`printf "$(eval_gettext "Reconfigure/Regenerate SSL root CA certificate.")"`"
PLUGIN_CONF_SUBMENU_ITEM_KEY_FORCE_REWRITE_SSL_MITM_CA="c"

PLUGIN_CONF_SUBMENU_ITEM_COMMAND_FORCE_SWITCH_R_BLISTS="SKIP_DEBCONF_QUESTIONS_CONFIG=1 FORCE_SWITCH_AUTOREFRESH_BLACKLISTS=true dpkg-reconfigure $PLUGIN_PACKAGE_NAME"
if [[ -s "/var/lib/debian-edu-router/d-e-r-p.c-f/autorefresh_blacklists.enabled" ]]; then
    PLUGIN_CONF_SUBMENU_ITEM_TEXT_FORCE_SWITCH_R_BLISTS="`printf "$(eval_gettext "[X] Switch task off: Auto-refresh of blacklists.")"`"
else
    PLUGIN_CONF_SUBMENU_ITEM_TEXT_FORCE_SWITCH_R_BLISTS="`printf "$(eval_gettext "[ ] Switch task on: Auto-refresh of blacklists.")"`"
fi
PLUGIN_CONF_SUBMENU_ITEM_KEY_FORCE_SWITCH_R_BLISTS="b"

PLUGIN_CONF_SUBMENU_ITEM_COMMAND_FORCE_SWITCH_R_GENIPS="SKIP_DEBCONF_QUESTIONS_CONFIG=1 FORCE_SWITCH_AUTOREGENERATE_IPS=true dpkg-reconfigure $PLUGIN_PACKAGE_NAME"
if [[ -s "/var/lib/debian-edu-router/d-e-r-p.c-f/autogenerate_IPs.enabled" ]]; then
    PLUGIN_CONF_SUBMENU_ITEM_TEXT_FORCE_SWITCH_R_GENIPS="`printf "$(eval_gettext "[X] Switch task off: Auto-regenerate Proxy*Site.* IPs.")"`"
else
    PLUGIN_CONF_SUBMENU_ITEM_TEXT_FORCE_SWITCH_R_GENIPS="`printf "$(eval_gettext "[ ] Switch task on: Auto-regenerate Proxy*Site.* IPs.")"`"
fi
PLUGIN_CONF_SUBMENU_ITEM_KEY_FORCE_SWITCH_R_GENIPS="i"

PLUGIN_CONF_SUBMENU_ITEM_COMMAND_MANUALLY_REFRESH_BLISTS="/usr/sbin/debian-edu-router_e2guardian-refresh-blacklists"
PLUGIN_CONF_SUBMENU_ITEM_TEXT_MANUALLY_REFRESH_BLISTS="`printf "$(eval_gettext "Refresh blacklists manually.")"`"
PLUGIN_CONF_SUBMENU_ITEM_KEY_MANUALLY_REFRESH_BLISTS="r"

PLUGIN_CONF_SUBMENU_ITEM_COMMAND_MANUALLY_REFRESH_GENIPS="/usr/sbin/debian-edu-router_refresh-generated-ips"
PLUGIN_CONF_SUBMENU_ITEM_TEXT_MANUALLY_REFRESH_GENIPS="`printf "$(eval_gettext "Regenerate Proxy*Site.* IPs.")"`"
PLUGIN_CONF_SUBMENU_ITEM_KEY_MANUALLY_REFRESH_GENIPS="g"

PLUGIN_CONF_SUBMENU_ITEM_COMMAND_EDIT_SELECTED_CATEGORIES="$(
)if ls /etc/debian-edu-router/e2guardian.d/selected_categories/* >/dev/null 2>&1; then $(
    )editor /etc/debian-edu-router/e2guardian.d/selected_categories/*; $(
)else $(
    )echo 'Please manually refresh blacklists ('r') and then try again.'; $(
)fi"
PLUGIN_CONF_SUBMENU_ITEM_TEXT_EDIT_SELECTED_CATEGORIES="`printf "$(eval_gettext "Edit the selection of blacklist categories.")"`"
PLUGIN_CONF_SUBMENU_ITEM_KEY_EDIT_SELECTED_CATEGORIES="e"
