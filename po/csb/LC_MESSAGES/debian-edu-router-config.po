# Kashubian translations for PACKAGE package.
# Copyright (C) 2022-2023 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Automatically generated, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: 2.13.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2025-03-10 11:28+0100\n"
"PO-Revision-Date: 2022-04-20 15:46+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: csb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../bin/debian-edu-router-loginmenu.sh:32
#, sh-format
msgid "Debian Edu Router"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:33
#, sh-format
msgid "Plugin"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:144
#, sh-format
msgid "Welcome to %s %s (%s) on %s"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:145
#, sh-format
msgid "WARNING: All networks are currently down. Check your configuration."
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:146
#, sh-format
msgid ""
"WARNING: LDAP connection is currently down.\n"
"Please check if SSL certificates are needed (they can be installed in the "
"plugin menu) and\n"
"check your configuration. Use debian-edu-router_ldapsearch as a test command."
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:149
#, sh-format
msgid "machine ID"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:151
#, sh-format
msgid "%s Menu"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:152
#, sh-format
msgid "Please select: "
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:153
#, sh-format
msgid "Hit <ENTER> to continue..."
msgstr ""

#. Open Shell
#: ../bin/debian-edu-router-loginmenu.sh:156
#, sh-format
msgid "Launch a shell session"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:157
#, sh-format
msgid "Starting shell '%s'..."
msgstr ""

#. Base Configuration
#. Base Configuration Submenu
#: ../bin/debian-edu-router-loginmenu.sh:160
#: ../bin/debian-edu-router-loginmenu.sh:181
#, sh-format
msgid "%s Base Configuration"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:161
#, sh-format
msgid "Entering %s base configuration submenu..."
msgstr ""

#. Tools Submenu
#: ../bin/debian-edu-router-loginmenu.sh:164
#, sh-format
msgid "%s Handy tools and tricks"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:166
#, sh-format
msgid "Opening tools menu..."
msgstr ""

#. IP statistics
#: ../bin/debian-edu-router-loginmenu.sh:168
#, sh-format
msgid "Run IP traffic statistics utility"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:169
#, sh-format
msgid "Starting IPTRAF traffic monitor..."
msgstr ""

#. HTOP
#: ../bin/debian-edu-router-loginmenu.sh:171
#, sh-format
msgid "Run htop utility"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:172
#, sh-format
msgid "Starting htop utility..."
msgstr ""

#. D-E-R Logview (mcview)
#: ../bin/debian-edu-router-loginmenu.sh:174
#, sh-format
msgid "Show log files"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:175
#, sh-format
msgid "Starting logview utility..."
msgstr ""

#. DNSMASQ DHCP leases
#: ../bin/debian-edu-router-loginmenu.sh:177
#, sh-format
msgid "Show DHCP leases (%s)"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:178
#, sh-format
msgid "Showing DHCP leases of dnsmasq services..."
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:182
#, sh-format
msgid "Configure %s base system entirely"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:183
#, sh-format
msgid "Configure %s network settings"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:184
#, sh-format
msgid "Configure %s firewall settings"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:185
#, sh-format
msgid "Configure %s services"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:186
#, sh-format
msgid "Configure everything..."
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:187
#, sh-format
msgid "Configure networking..."
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:188
#, sh-format
msgid "Configure firewall settings..."
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:189
#, sh-format
msgid "Configure services..."
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:190
#, sh-format
msgid "Back to plugin menu"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:191
#, sh-format
msgid "Back to main menu"
msgstr ""

#. Plugin Configuration
#. Plugins Configuration Submenu
#: ../bin/debian-edu-router-loginmenu.sh:194
#: ../bin/debian-edu-router-loginmenu.sh:198
#, sh-format
msgid "%s Plugin Configuration"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:195
#, sh-format
msgid "Entering %s plugin configuration submenu..."
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:199
#, sh-format
msgid "Plugins marked with '[X]' are enabled, '[ ]' means they are disabled."
msgstr ""

#. Reboot stuff
#: ../bin/debian-edu-router-loginmenu.sh:202
#, sh-format
msgid "Reboot system"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:203
#, sh-format
msgid "Are you sure you want to reboot the system? Type in 'imsure':"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:204
#, sh-format
msgid ""
"WARNING!\n"
"Make sure you still have (physical) access to the machine in case the boot\n"
"process fails. If this is a VM Guest, make sure you have access to the\n"
"VM Host. An internet downage can easily ruin your day!\n"
"You have been warned."
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:209
#: ../bin/debian-edu-router-loginmenu.sh:221
#, sh-format
msgid "imsure"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:211
#, sh-format
msgid "OK. Rebooting %s in %s seconds!"
msgstr ""

#. Shutdown stuff
#: ../bin/debian-edu-router-loginmenu.sh:214
#, sh-format
msgid "Shutdown system"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:215
#, sh-format
msgid "Are you sure you want to shut down the system? Type in 'imsure':"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:216
#, sh-format
msgid ""
"WARNING!\n"
"Make sure you still have (physical) access to the machine!\n"
"If this is a VM Guest, make sure you have access to the\n"
"VM Host. An internet downage can easily ruin your day!\n"
"You have been warned."
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:223
#, sh-format
msgid "OK. Shutting down %s in %s seconds!"
msgstr ""

#. Debug Mode
#: ../bin/debian-edu-router-loginmenu.sh:226
#, sh-format
msgid "Toggle debug messages on"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:227
#, sh-format
msgid "Toggle debug messages off"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:233
#, sh-format
msgid "Enabling debug messages..."
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:234
#, sh-format
msgid "Reconfiguring %s + Plugins..."
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:235
#, sh-format
msgid "Disabling debug messages..."
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:236
#, sh-format
msgid "Toggling..."
msgstr ""

#. Quit
#: ../bin/debian-edu-router-loginmenu.sh:239
#, sh-format
msgid "Quit menu and logout as user '%s'"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:240
#, sh-format
msgid "Return back to plugin menu"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:241
#, sh-format
msgid "Return back to main menu"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:242
#, sh-format
msgid "Quit"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:244
#, sh-format
msgid "Saving changes to /etc via 'etckeeper'..."
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:245
#, sh-format
msgid "Loginmenu was used to configure '%s'."
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:474
#, sh-format
msgid "%s » Open configuration menu"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:476
#, sh-format
msgid "%s » Configure entirely"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:511
#: ../bin/debian-edu-router-loginmenu.sh:557
#, sh-format
msgid "Configuring %s now..."
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:532
#, sh-format
msgid "Configuration of the %s"
msgstr ""
