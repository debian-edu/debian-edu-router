# Crimean Tatar translations for PACKAGE package.
# Copyright (C) 2024 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Automatically generated, 2024.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2025-03-10 11:28+0100\n"
"PO-Revision-Date: 2024-08-13 08:53+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: crh\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ASCII\n"
"Content-Transfer-Encoding: 8bit\n"

#. Set product name defaults "Debian Edu Router Plugin"
#. Normally these settings should be defined in /etc/debian-edu/router.conf.d/
#: ../conf/debian-edu-router-plugins/d-e-r-p.krb5-connector.sh:35
#, sh-format
msgid "Debian Edu Router"
msgstr ""

#: ../conf/debian-edu-router-plugins/d-e-r-p.krb5-connector.sh:36
#, sh-format
msgid "Plugin"
msgstr ""

#. Debian Edu Router Plugin: Krb5 Connector
#: ../conf/debian-edu-router-plugins/d-e-r-p.krb5-connector.sh:39
#, sh-format
msgid "%s %s: Krb5 Connector"
msgstr ""

#: ../conf/debian-edu-router-plugins/d-e-r-p.krb5-connector.sh:40
#, sh-format
msgid "Provides the possibility to authenticate using Kerberos tickets."
msgstr ""

#: ../conf/debian-edu-router-plugins/d-e-r-p.krb5-connector.sh:58
#, sh-format
msgid "Configure entirely"
msgstr ""

#: ../conf/debian-edu-router-plugins/d-e-r-p.krb5-connector.sh:62
#, sh-format
msgid "Toggle Krb5 connector's functionality on/off"
msgstr ""

#: ../conf/debian-edu-router-plugins/d-e-r-p.krb5-connector.sh:66
#, sh-format
msgid "Install HTTP proxy service ticket on Kerberos server."
msgstr ""
