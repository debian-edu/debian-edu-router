# Croatian translations for PACKAGE package.
# Copyright (C) 2023 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Automatically generated, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2025-03-10 11:28+0100\n"
"PO-Revision-Date: 2023-10-14 18:52+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: hr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ASCII\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. Set product name defaults "Debian Edu Router Plugin"
#. Normally these settings should be defined in /etc/debian-edu/router.conf.d/
#: ../conf/debian-edu-router-plugins/d-e-r-p.ldap-connector.sh:35
#, sh-format
msgid "Debian Edu Router"
msgstr ""

#: ../conf/debian-edu-router-plugins/d-e-r-p.ldap-connector.sh:36
#, sh-format
msgid "Plugin"
msgstr ""

#. Debian Edu Router Plugin: Content filter
#: ../conf/debian-edu-router-plugins/d-e-r-p.ldap-connector.sh:39
#, sh-format
msgid "%s %s: LDAP/AD Connector"
msgstr ""

#: ../conf/debian-edu-router-plugins/d-e-r-p.ldap-connector.sh:40
#, sh-format
msgid "Retrieve configurations, filter lists, etc. from an LDAP server"
msgstr ""

#: ../conf/debian-edu-router-plugins/d-e-r-p.ldap-connector.sh:58
#, sh-format
msgid "Configure entirely"
msgstr ""

#: ../conf/debian-edu-router-plugins/d-e-r-p.ldap-connector.sh:62
#, sh-format
msgid "Toggle LDAP/AD connector's functionality on/off"
msgstr ""

#: ../conf/debian-edu-router-plugins/d-e-r-p.ldap-connector.sh:66
#, sh-format
msgid "Configure the LDAP server connection"
msgstr ""

#: ../conf/debian-edu-router-plugins/d-e-r-p.ldap-connector.sh:70
#, sh-format
msgid "Configure the LDAP user/group mappings"
msgstr ""

#: ../conf/debian-edu-router-plugins/d-e-r-p.ldap-connector.sh:74
#, sh-format
msgid "Install SSL certificates for LDAP"
msgstr ""

#: ../conf/debian-edu-router-plugins/d-e-r-p.ldap-connector.sh:79
#, sh-format
msgid "[X] Switch task off: Auto-refresh of LDAP filterlists"
msgstr ""

#: ../conf/debian-edu-router-plugins/d-e-r-p.ldap-connector.sh:81
#, sh-format
msgid "[ ] Switch task on: Auto-refresh of LDAP filterlists"
msgstr ""

#: ../conf/debian-edu-router-plugins/d-e-r-p.ldap-connector.sh:86
#, sh-format
msgid "Refresh LDAP filterlists manually."
msgstr ""
