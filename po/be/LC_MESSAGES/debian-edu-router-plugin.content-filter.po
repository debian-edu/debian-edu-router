# Belarusian translations for PACKAGE package.
# Copyright (C) 2023 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Automatically generated, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: 2.13.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2025-03-10 11:28+0100\n"
"PO-Revision-Date: 2023-10-06 21:24+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: be\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ASCII\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. Set product name defaults "Debian Edu Router Plugin"
#: ../conf/debian-edu-router-plugins/d-e-r-p.content-filter.sh:34
#, sh-format
msgid "Debian Edu Router"
msgstr ""

#: ../conf/debian-edu-router-plugins/d-e-r-p.content-filter.sh:35
#, sh-format
msgid "Plugin"
msgstr ""

#. Debian Edu Router Plugin: Content filter
#: ../conf/debian-edu-router-plugins/d-e-r-p.content-filter.sh:38
#, sh-format
msgid "%s %s: Content filter"
msgstr ""

#: ../conf/debian-edu-router-plugins/d-e-r-p.content-filter.sh:39
#, sh-format
msgid ""
"This plugin adds an squid/e2guardian based content filtering system for the "
"internal networks."
msgstr ""

#: ../conf/debian-edu-router-plugins/d-e-r-p.content-filter.sh:57
#, sh-format
msgid "Configure entirely"
msgstr ""

#: ../conf/debian-edu-router-plugins/d-e-r-p.content-filter.sh:61
#, sh-format
msgid "Toggle content filter's functionality on/off"
msgstr ""

#: ../conf/debian-edu-router-plugins/d-e-r-p.content-filter.sh:65
#, sh-format
msgid "Reconfigure/Regenerate SSL root CA certificate."
msgstr ""

#: ../conf/debian-edu-router-plugins/d-e-r-p.content-filter.sh:70
#, sh-format
msgid "[X] Switch task off: Auto-refresh of blacklists."
msgstr ""

#: ../conf/debian-edu-router-plugins/d-e-r-p.content-filter.sh:72
#, sh-format
msgid "[ ] Switch task on: Auto-refresh of blacklists."
msgstr ""

#: ../conf/debian-edu-router-plugins/d-e-r-p.content-filter.sh:78
#, sh-format
msgid "[X] Switch task off: Auto-regenerate Proxy*Site.* IPs."
msgstr ""

#: ../conf/debian-edu-router-plugins/d-e-r-p.content-filter.sh:80
#, sh-format
msgid "[ ] Switch task on: Auto-regenerate Proxy*Site.* IPs."
msgstr ""

#: ../conf/debian-edu-router-plugins/d-e-r-p.content-filter.sh:85
#, sh-format
msgid "Refresh blacklists manually."
msgstr ""

#: ../conf/debian-edu-router-plugins/d-e-r-p.content-filter.sh:89
#, sh-format
msgid "Regenerate Proxy*Site.* IPs."
msgstr ""

#: ../conf/debian-edu-router-plugins/d-e-r-p.content-filter.sh:98
#, sh-format
msgid "Edit the selection of blacklist categories."
msgstr ""
