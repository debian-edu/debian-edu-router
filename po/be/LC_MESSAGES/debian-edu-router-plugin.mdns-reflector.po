# Belarusian translations for PACKAGE package.
# Copyright (C) 2023 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Automatically generated, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: 2.13.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2025-03-10 11:28+0100\n"
"PO-Revision-Date: 2023-05-09 11:22+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: be\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ASCII\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. Set product name defaults "Debian Edu Router Plugin"
#: ../conf/debian-edu-router-plugins/d-e-r-p.mdns-reflector.sh:34
#, sh-format
msgid "Debian Edu Router"
msgstr ""

#: ../conf/debian-edu-router-plugins/d-e-r-p.mdns-reflector.sh:35
#, sh-format
msgid "Plugin"
msgstr ""

#. Debian Edu Router Plugin: mDNS reflector
#: ../conf/debian-edu-router-plugins/d-e-r-p.mdns-reflector.sh:38
#, sh-format
msgid "%s %s: mDNS reflector"
msgstr ""

#: ../conf/debian-edu-router-plugins/d-e-r-p.mdns-reflector.sh:39
#, sh-format
msgid "This package adds an mDNS reflector service to Debian Edu Router."
msgstr ""

#: ../conf/debian-edu-router-plugins/d-e-r-p.mdns-reflector.sh:49
#, sh-format
msgid "Configure entirely"
msgstr ""

#: ../conf/debian-edu-router-plugins/d-e-r-p.mdns-reflector.sh:53
#, sh-format
msgid "Toggle mDNS reflector's functionality on/off"
msgstr ""
