# Pashto translations for PACKAGE package.
# Copyright (C) 2022-2023 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Automatically generated, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: 2.13.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2025-03-10 11:28+0100\n"
"PO-Revision-Date: 2022-04-20 15:46+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: ps\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../fai/config/class/40-parse-profiles.sh:174
#, sh-format
msgid "Name:"
msgstr ""

#: ../fai/config/class/40-parse-profiles.sh:176
#, sh-format
msgid "Classes:"
msgstr ""

#: ../fai/config/class/40-parse-profiles.sh:182
#: ../fai/config/class/41-warning.sh:43
#, sh-format
msgid "FAI - Fully Automatic Installation"
msgstr ""

#: ../fai/config/class/40-parse-profiles.sh:183
#, sh-format
msgid "Debian Edu Router Installation"
msgstr ""

#: ../fai/config/class/40-parse-profiles.sh:186
#, sh-format
msgid ""
"\\nPlease, select your Debian Edu Router installation profile.\\n\\nThe "
"profile will determine installation properties such as system language, "
"network setup, available features, etc.\\n\\n\\n"
msgstr ""

#: ../fai/config/class/40-parse-profiles.sh:196
#, sh-format
msgid "No profile selected."
msgstr ""

#: ../fai/config/class/40-parse-profiles.sh:199
#, sh-format
msgid "Description of all profiles"
msgstr ""

#: ../fai/config/class/41-warning.sh:44
#, sh-format
msgid ""
"\\n\\n        If you continue,       \\n   all your data on the disk   "
"\\n                               \\n|\\Zr\\Z1       WILL BE DESTROYED     "
"\\Z0\\Zn|\\n\\n"
msgstr ""

#: ../fai/config/hooks/install.DEBIAN_EDU_ROUTER.sh:40
#, sh-format
msgid "Not enough Network Interfaces"
msgstr ""

#: ../fai/config/hooks/install.DEBIAN_EDU_ROUTER.sh:41
#: ../fai/config/hooks/install.DEFAULT.sh:42
#: ../fai/config/hooks/install.DEFAULT.sh:44
#: ../fai/config/hooks/install.DEFAULT.sh:51
#: ../fai/config/hooks/install.DEFAULT.sh:78
#: ../fai/config/hooks/install.DEFAULT.sh:95
#: ../fai/config/hooks/install.DEFAULT.sh:102
#: ../fai/config/hooks/install.DEFAULT.sh:133
#, sh-format
msgid "Configure Base System"
msgstr ""

#: ../fai/config/hooks/install.DEBIAN_EDU_ROUTER.sh:42
#, sh-format
msgid "There are not enough network interfaces available on this system."
msgstr ""

#: ../fai/config/hooks/install.DEBIAN_EDU_ROUTER.sh:42
#, sh-format
msgid "Number of interfaces required:"
msgstr ""

#: ../fai/config/hooks/install.DEBIAN_EDU_ROUTER.sh:46
#, sh-format
msgid "Not enough network cards. Cancelling installation..."
msgstr ""

#: ../fai/config/hooks/install.DEFAULT.sh:42
#: ../fai/config/hooks/install.DEFAULT.sh:50
#: ../fai/config/hooks/install.DEFAULT.sh:77
#, sh-format
msgid "Password for super-user 'root'"
msgstr ""

#: ../fai/config/hooks/install.DEFAULT.sh:43
#, sh-format
msgid ""
"Please provide a secure password for the super-user 'root' (system "
"administrator account)."
msgstr ""

#: ../fai/config/hooks/install.DEFAULT.sh:43
#, sh-format
msgid "Please enter the root password:"
msgstr ""

#: ../fai/config/hooks/install.DEFAULT.sh:44
#, sh-format
msgid "Password for super-user 'root' [again]"
msgstr ""

#: ../fai/config/hooks/install.DEFAULT.sh:45
#, sh-format
msgid "Please re-enter the same password for the super-user 'root' account."
msgstr ""

#: ../fai/config/hooks/install.DEFAULT.sh:45
#, sh-format
msgid "Please enter the root password again:"
msgstr ""

#: ../fai/config/hooks/install.DEFAULT.sh:48
#: ../fai/config/hooks/install.DEFAULT.sh:99
#, sh-format
msgid "Yes"
msgstr ""

#: ../fai/config/hooks/install.DEFAULT.sh:49
#: ../fai/config/hooks/install.DEFAULT.sh:100
#: ../fai/config/hooks/install.GATEWAY.sh:80
#: ../fai/config/hooks/install.GATEWAY.sh:119
#: ../fai/config/hooks/install.GATEWAY.sh:245
#, sh-format
msgid "Cancel installation"
msgstr ""

#: ../fai/config/hooks/install.DEFAULT.sh:52
#, sh-format
msgid "Entering passwords cancelled. Try again?"
msgstr ""

#: ../fai/config/hooks/install.DEFAULT.sh:56
#, sh-format
msgid "Installation cancelled while entering root password."
msgstr ""

#: ../fai/config/hooks/install.DEFAULT.sh:68
#, sh-format
msgid "Root password hash is:"
msgstr ""

#: ../fai/config/hooks/install.DEFAULT.sh:79
#, sh-format
msgid "Passwords do not match, please try again."
msgstr ""

#: ../fai/config/hooks/install.DEFAULT.sh:95
#: ../fai/config/hooks/install.DEFAULT.sh:101
#: ../fai/config/hooks/install.DEFAULT.sh:134
#, sh-format
msgid "Hostname Setup"
msgstr ""

#: ../fai/config/hooks/install.DEFAULT.sh:96
#, sh-format
msgid "No hostname for this machine could be detected."
msgstr ""

#: ../fai/config/hooks/install.DEFAULT.sh:96
#, sh-format
msgid "Please enter a hostname:"
msgstr ""

#: ../fai/config/hooks/install.DEFAULT.sh:103
#, sh-format
msgid "Entering hostname cancelled. Try again?"
msgstr ""

#: ../fai/config/hooks/install.DEFAULT.sh:107
#, sh-format
msgid "Installation cancelled while entering hostname."
msgstr ""

#: ../fai/config/hooks/install.DEFAULT.sh:124
#, sh-format
msgid "Hostname set to:"
msgstr ""

#: ../fai/config/hooks/install.DEFAULT.sh:135
#, sh-format
msgid "Hostname '%s' invalid, please try again."
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:78
#: ../fai/config/hooks/install.GATEWAY.sh:97
#: ../fai/config/hooks/install.GATEWAY.sh:119
#: ../fai/config/hooks/install.GATEWAY.sh:139
#: ../fai/config/hooks/install.GATEWAY.sh:148
#: ../fai/config/hooks/install.GATEWAY.sh:158
#: ../fai/config/hooks/install.GATEWAY.sh:167
#: ../fai/config/hooks/install.GATEWAY.sh:177
#: ../fai/config/hooks/install.GATEWAY.sh:188
#: ../fai/config/hooks/install.GATEWAY.sh:202
#: ../fai/config/hooks/install.GATEWAY.sh:234
#: ../fai/config/hooks/install.GATEWAY.sh:237
#: ../fai/config/hooks/install.GATEWAY.sh:243
#, sh-format
msgid "Configure Internet Access"
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:79
#, sh-format
msgid "No network adapter connected!"
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:80
#: ../fai/config/hooks/install.GATEWAY.sh:245
#, sh-format
msgid "Try again"
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:81
#: ../fai/config/hooks/install.GATEWAY.sh:100
#, sh-format
msgid ""
"Please plugin a network cable to the uplink network interface and try again."
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:98
#, sh-format
msgid "Please choose uplink [external] network interface"
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:99
#, sh-format
msgid "Select"
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:99
#, sh-format
msgid "Re-scan network interfaces"
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:107
#, sh-format
msgid "Uplink NIC chosen:"
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:119
#: ../fai/config/hooks/install.GATEWAY.sh:140
#, sh-format
msgid "External IPv4 address"
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:120
#, sh-format
msgid "Please specify the external IPv4 address of this system."
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:128
#, sh-format
msgid "Manual network setup cancelled."
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:141
#: ../fai/config/hooks/install.GATEWAY.sh:179
#, sh-format
msgid "IPv4 address '%s' is of invalid format, please try again."
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:148
#: ../fai/config/hooks/install.GATEWAY.sh:159
#, sh-format
msgid "External IPv4 netmask"
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:148
#: ../fai/config/hooks/install.GATEWAY.sh:167
#: ../fai/config/hooks/install.GATEWAY.sh:188
#, sh-format
msgid "Back"
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:149
#, sh-format
msgid "Please specify the external interface's IPv4 netmask."
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:160
#, sh-format
msgid "IPv4 netmask '%s' is of invalid format, please try again."
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:167
#: ../fai/config/hooks/install.GATEWAY.sh:178
#, sh-format
msgid "External IPv4 gateway"
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:168
#, sh-format
msgid "Please specify the external interface's IPv4 gateway address."
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:188
#: ../fai/config/hooks/install.GATEWAY.sh:203
#, sh-format
msgid "Upstream DNS servers"
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:189
#, sh-format
msgid ""
"Please specify available IPv4 DNS servers on the external network. Use "
"commas or blanks to separate several DNS server addresses."
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:204
#, sh-format
msgid "DNS server's IPv4 address '%s' is of invalid format, please try again."
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:234
#: ../fai/config/hooks/install.GATEWAY.sh:237
#, sh-format
msgid "Manual IPv4 network setup [required]"
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:235
#, sh-format
msgid ""
"Internet access still fails using the IPv4 networking data you entered "
"manually."
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:235
#, sh-format
msgid "Do you want to retry setting up IPv4 internet access manually?"
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:238
#, sh-format
msgid "The system failed to obtain an IPv4 address via DHCP automatically."
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:238
#, sh-format
msgid "Do you want to setup IPv4 internet access manually?"
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:244
#, sh-format
msgid "No IPv4 network / internet access available!"
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:247
#, sh-format
msgid "Fix this problem [Ctrl-Alt-F2] and try again."
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:251
#, sh-format
msgid "Uplink network setup cancelled, installation can't proceed."
msgstr ""

#: ../fai/config/hooks/install.GATEWAY.sh:323
#, sh-format
msgid "Uplink ifupdown config will look like this:"
msgstr ""
